<%@page import="ch.ethz.inf.dbproject.model.Conviction"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>
<!DOCTYPE html>
<html>

<h1>Case Details</h1>

<%
if (user != null) {
%>
<form action="Case" method="post">
	<% if(request.getAttribute("errorMsg") != null) { %> ${errorMsg} <br> <% } %>
	Case Nr.: 	 <input type="text" name="caseNr" value="${caseNr}" readonly><br>
	Title: 		 <input type="text" name="title" value="${title}" ${readonly}><br>
	Description: <input type="text" name="description" value="${description}" ${readonly}><br>
	Location: 	 <input type="text" name="location" value="${location}" ${readonly}><br>
	Category:
	<% if(request.getAttribute("readonly").equals("")) { %>
	    <select name="categoryName">
	    	<option>null</option>
		    <% for (Category c: categories) { %>
		    	<% if(c.getName().equals(request.getAttribute("categoryName"))) { %>
		      		<option selected><%= c.getName() %></option>
		    	<% } else { %>
		      		<option><%= c.getName() %></option>
		    	<% } %>
		    <% } %>
	    </select>
    <% } else { %>
		<input type="text" name="categoryName" value="${categoryName}" ${readonly}>
    <% } %>
    <br>
	Date: <input type="date" name="date" value="${date}" ${readonly}><br>
	Creator: 	 <input type="text" name="creator" value="${creator}" readonly><br>
	Open: 		 <input type="checkbox" name="open" readonly value="ON" ${open}><br>
	 		     <input type="hidden" name="action" value="add_comment" />
				 <input type="hidden" name="username" value="<%= user.getUsername() %>" />
	Add Comment:
	<br/>
	<textarea rows="4" cols="50" name="comment" ${readonly}>${note}</textarea>
	<br/>
	<input type="submit" value="Save" name="savebutton" /> ${state}
	<input type="submit" value= "${button}" name="closebutton" />
</form>
	
<%
}else{
%>
<form action="Case" method="post">
Case Nr.: 	 <input type="text" name="caseNr" value="${caseNr}" readonly><br>
	Title: 		 <input type="text" name="title" value="${title}" readonly><br>
	Description: <input type="text" name="description" value="${description}" readonly><br>
	Location: 	 <input type="text" name="location" value="${location}" readonly><br>
	Category: 	 <input type="text" name="categoryName" value="${categoryName}" readonly><br>
	Date: 		 <input type="text" name="date" value="${date}" readonly><br>
	Creator: 	 <input type="text" name="creator" value="${creator}" readonly><br>
	Open: 		 <input type="checkbox" name="open" value="ON" ${open} readonly><br>
</form>
<%
}
%>

<h2>Notes</h2>
<%= request.getAttribute("commentTable") %>

<h2>Linked POI</h2>
<%= request.getAttribute("lpoiTable") %>

<br>

<% if (request.getAttribute("copen").equals("1")) { %>	
	<% if(user != null) { %>
		<h2>Unlinked POI</h2>
		<%=request.getAttribute("poiTable") %>	
	<% } %>	
<% } else { %>
	<h2>Convictions</h2>
	<%=request.getAttribute("convTable") %>	
	<% Conviction c = (Conviction) request.getAttribute("convEdit");
	   if (c != null) { %>
		<h2>Convictions</h2>
		<form action="Case" method="POST">
			<input type="text" name="convEdited" value="<%= c.getPoiId() %>" readonly>
			<input type="hidden" name="caseNr" value="<%= c.getCaseNr() %>"><br>
 		 	Date: <input type="date" name="date" value="<%= c.getDateString() %>" readonly><br>
 		 	End date: <input type="date" name="endDate" value="<%= c.getEndDateString() %>"><br>
			<textarea rows="4" cols="50" name="reason"><%= c.getReason() %></textarea><br>
			<input type="submit" value="Edit">
		</form>
	<% } %>
<% } %>	

</html>
<%@ include file="Footer.jsp"%>