<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>
<%@page import="ch.ethz.inf.dbproject.model.Category"%>

<h1>Categories</h1>

<hr/>

<%= request.getAttribute("table") %>

<hr/>
<% if (user != null) { %>
<form method="POST" action="Categories">
	Category name:  <input type="text" name="name"><br>
	Super category:
    <select name="super">
    	<option>null</option>
	    <% for (Category c: categories) { %>
	      <option><%= c.getName() %></option>
	    <% } %>
    </select>
	<input type="submit" value="Add" name="add"/>
</form>
<% } %>

<%@ include file="Footer.jsp" %>