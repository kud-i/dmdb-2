<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>
<!DOCTYPE html>
<h1>Add Case</h1>

<hr/>

<form action="CreateCase" method="post">
	Title: 		 <input type="text" name="title"><br>
	Description: <input type="text" name="description"><br>
	Location: 	 <input type="text" name="location"><br>
	Category:
		<select name="category">
	    <% for (Category c: categories) { %>
	      <option><%= c.getName() %></option>
	    <% } %>
	    </select><br>
	Date: 		 <input type="date" name="date"><br>
	 		     <input type="hidden" name="action" value="add_comment" />
				 <input type="hidden" name="username" value="<%= user.getUsername() %>" />
	Add Comment:
	<br />
	<textarea rows="4" cols="50" name="comment"></textarea>
	<br />
	<input type="submit" value="Submit"/> ${state}
</form>

<hr/>

<hr/>

<%@ include file="Footer.jsp" %>