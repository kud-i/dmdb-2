<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>
<!DOCTYPE html>
<h1>Add Person Of Interest</h1>


<form method="post" action="CreatePoi">
<div>
	<input type="hidden" name="toAdd" value="firstName" />
	 First Name:
	<input type="text" name="firstName" />
</div>

<div>
	<input type="hidden" name="toAdd" value="lastName" />
	Last Name:
	<input type="text" name="lastName" />
</div>

<div>
	<input type="hidden" name="toAdd" value="birthdate" />
	Birthdate:
	<input type="date" name="birthdate" />
</div>

${error}<br />

<input type="submit" value="Add" title="Add new Poi" />

</form>



<%@ include file="Footer.jsp" %>
