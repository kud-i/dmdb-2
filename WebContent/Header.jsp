<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="ch.ethz.inf.dbproject.model.Category"%>
<%@page import="ch.ethz.inf.dbproject.util.CategoryManagement"%>
<% final List<Category> categories = CategoryManagement.getCategories();%>
<%@page import="ch.ethz.inf.dbproject.model.User"%>
<%@page import="ch.ethz.inf.dbproject.util.UserManagement"%>
<% final User user = (User) session.getAttribute(UserManagement.SESSION_USER);%>

<!-- Orginaler Header <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> -->
<!DOCTYPE html>
<html>
	<head>
	    <link href="style.css" rel="stylesheet" type="text/css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>knackiVZ</title>
	</head>

	<body>

		<!-- Header -->
		
		<table id="masterTable" cellpadding="0" cellspacing="0">
			<tr>
				<th id="masterHeader" colspan="2">
					<h1>knackiVZ</h1>
					Project by Felix, Tim, Michael; 
				</th>
			</tr>
			<tr id="masterContent">
			
				<td id="masterContentMenu">
					<div class="menuDiv1"></div>
					<div class="menuDiv1"><a href="Home">Home</a></div>
					<div class="menuDiv1"><a href="Cases">All cases</a></div>
					<div class="menuDiv2"><a href="Cases?filter=open">Open</a></div>
					<div class="menuDiv2"><a href="Cases?filter=closed">Closed</a></div>
					<div class="menuDiv2"><a href="Cases?filter=recent">Recent</a></div>
					<div class="menuDiv2"><a href="Cases?filter=oldest">Oldest Unsolved</a></div>
					<% if (user != null) { %>
						<div class="menuDiv2"><a href="Cases?filter=own">Own cases</a></div>
					<% } %>
					<div class="menuDiv1"><a href="Categories">Categories</a></div>
					<% for (Category c: categories) { %>
						<%=
							String.format(
								"<div class=\"menuDiv%d\"><a href=\"Cases?category=%s\">%s</a></div>",
								c.getLevel() + 1, c.getEncodedName(), c.getName())
						%>
					<% } %>
					<div class="menuDiv1"><a href="Pois">Persons of Interest</a></div>
					<div class="menuDiv1"><a href="Search">Search</a></div>
					<% if (user != null) { %>
						<div class="menuDiv1"><a href="User">User Profile</a></div>
						<div class="menuDiv1"><a href="User?logout=logout">Logout</a></div>
					<% } else { %>
						<div class="menuDiv1"><a href="User">Login/Register</a></div>
						<div class="menuDiv1"><a href="User?login=login&username=Bond&password=007">Login as test user</a></div>
					<% } %>
					
				</td>
				
				<td id="masterContentPlaceholder">
				
		