<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>

<h1>Search for anything</h1>

<p>
  Just enter a date or a (partial) name of a POI, a case or a category
  and we will help find you what you want.
</p>

<hr/>

<form method="get" action="Search">
<div>
	<input type="hidden" name="filter" value="case" />
	Search for Case:
	<input type="text" name="case" value="${caset}"/>
	<input type="submit" value="Search" title="Search for case" />
</div>
</form>

<hr/>

<form method="get" action="Search">
<div>
	<input type="hidden" name="filter" value="poi" />
	Search for POI:
	<input type="text" name="poi" value="${poit}"/>
	<input type="submit" value="Search" title="Search by POI" />
</div>
</form>

<hr/>

<form method="get" action="Search">
<div>
	<input type="hidden" name="filter" value="conviction" />
	Search for Conviction:
	<input type="text" name="conviction" value="${cont}"/>
	<input type="submit" value="Search" title="Search for conviction" />
</div>
</form>

<hr/>

<%=	session.getAttribute("results")%>

<hr/>

<%@ include file="Footer.jsp" %>