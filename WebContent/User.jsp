<%@page import="ch.ethz.inf.dbproject.UserServlet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>

<% 
if ((Boolean) session.getAttribute(UserServlet.SESSION_USER_LOGGED_IN)) {
	// User is logged in. Display the details:
%>

<h2>Your Account</h2>

<%= session.getAttribute(UserServlet.SESSION_USER_DETAILS) %>

<%
	
} else {
	// User not logged in. Display the login form.
%>

<h2>Login or Register</h2>

<form action="User" method="GET">
	<%= request.getAttribute("login-error") %> <br>
	Username
	<input type="text" name="username" value="" /> <br>
	Password
	<input type="password" name="password" value="" /> <br>
	<input type="submit" name="login" value="Login" />
	<input type="submit" name="register" value="Register" />
</form>

<%
}
%>

<%@ include file="Footer.jsp" %>