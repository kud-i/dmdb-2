package ch.ethz.inf.dbproject;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.Conviction;
import ch.ethz.inf.dbproject.model.Comment;
import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.Case;
import ch.ethz.inf.dbproject.model.Poi;
import ch.ethz.inf.dbproject.model.User;
import ch.ethz.inf.dbproject.util.UserManagement;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

/**
 * Servlet implementation class of Case Details Page
 */
@WebServlet(description = "Displays a specific case.", urlPatterns = { "/Case" })
public final class CaseServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = new DatastoreInterface();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CaseServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

		final HttpSession session = request.getSession(true);

		final String nrString = request.getParameter("caseNr");
		final User user = UserManagement.getCurrentlyLoggedInUser(session);
		
		if (nrString == null) {
			this.getServletContext().getRequestDispatcher("/Cases").forward(request, response);
		}

		try {
			final Integer nr = Integer.parseInt(nrString);
			final Case aCase = this.dbInterface.getCaseByNr(nr);
			
			/*******************************************************
			 * Construct a table to present all properties of a case
			 *******************************************************/
			final BeanTableHelper<Comment> cTable = new BeanTableHelper<Comment>(
					"cases" 		/* The table html id property */,
					"casesTable" /* The table html class property */,
					Comment.class 	/* The class of the objects (rows) that will be displayed */
			);

			// Add columns to the new table
			
			cTable.addBeanColumn("Note", "comment");
			cTable.addBeanColumn("User", "username");

			cTable.addObjects(this.dbInterface.getCNotesbyCaseNr(aCase.getCaseNr()));
			//table.setVertical(true);

			request.setAttribute("commentTable", cTable);
			
			if (user != null) {
				Comment c = dbInterface.getCNotebyCaseNrAndUser(aCase.getCaseNr(),user.getUsername());
				if(c != null) {
					request.setAttribute("note", c.getComment());
				} else {
					request.setAttribute("note", "");
				}
			}
			
			//Linked Poi
			final BeanTableHelper<Poi> lpoiTable = new BeanTableHelper<Poi>(
					"lpoi" 		/* The table html id property */,
					"casesTable" /* The table html class property */,
					Poi.class 	/* The class of the objects (rows) that will be displayed */
			);
			
			// Add columns to the new table
			
			lpoiTable.addBeanColumn("POI ID", "poiId");
			lpoiTable.addBeanColumn("First Name", "firstName");
			lpoiTable.addBeanColumn("Last Name", "lastName");
			lpoiTable.addBeanColumn("Birthdate", "birthdate");
			
			if (aCase.getOpen() && user != null) {
				lpoiTable.addLinkColumn(""	/* The header. We will leave it empty */,
					"Unlink POI" 	/* What should be displayed in every row */,
					"UnlinkPoi?caseNr="+ aCase.getCaseNr() +"&poiId=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
					"poiId" 	);
			}

			lpoiTable.addObjects(this.dbInterface.getPoiByCaseNr(aCase.getCaseNr()));
			
			request.setAttribute("lpoiTable", lpoiTable);
			
			//not linked Poi
			final BeanTableHelper<Poi> poiTable = new BeanTableHelper<Poi>(
					"poi", "casesTable", Poi.class);
			
			// Add columns to the new table
			poiTable.addBeanColumn("POI ID", "poiId");
			poiTable.addBeanColumn("First Name", "firstName");
			poiTable.addBeanColumn("Last Name", "lastName");
			poiTable.addBeanColumn("Birthdate", "birthdate");
			
			if (aCase.getOpen()) {
				poiTable.addLinkColumn("", "Link POI", "LinkPoi?caseNr="+ aCase.getCaseNr() + "&poiId=", "poiId");
				poiTable.addObjects(dbInterface.getNotLinkedPoisbyCaseNr(aCase.getCaseNr()));
				request.setAttribute("poiTable", poiTable);
			} else {
				List<Conviction> convictions = dbInterface.getConvictionsByCaseNr(aCase.getCaseNr());
				
				final BeanTableHelper<Conviction> convTable = new BeanTableHelper<Conviction>(
						"convictions", "casesTable", Conviction.class);

				convTable.addBeanColumn("PoiId", "poiId");
				convTable.addBeanColumn("Date", "dateString");
				convTable.addBeanColumn("End Date", "endDateString");
				convTable.addBeanColumn("Reason", "reason");
				if(user != null) {
					convTable.addLinkColumn("", "Edit", "Case?caseNr=" + aCase.getCaseNr() + "&convEdit=", "poiId");
				}
				
				convTable.addObjects(convictions);

				request.setAttribute("convTable", convTable);
				
				String convEdit = request.getParameter("convEdit");
				if(convEdit != null) {
					int poiId = Integer.parseInt(convEdit);
					request.setAttribute("convEdit", dbInterface.getConviction(aCase.getCaseNr(), poiId));
				}
			}

			request.setAttribute("caseNr", aCase.getCaseNr());
			request.setAttribute("title", aCase.getTitle());
			request.setAttribute("description", aCase.getDescription());
			request.setAttribute("location", aCase.getLocation());
			request.setAttribute("categoryName", aCase.getCategoryName());
			request.setAttribute("date", aCase.getDate());
			request.setAttribute("creator", aCase.getUsername());
			
			if (aCase.getOpen()) {
				request.setAttribute("copen", "1");
				request.setAttribute("readonly","");
				request.setAttribute("open","checked");
				request.setAttribute("button","Close");
			} else { 
				request.setAttribute("copen", "0");
				request.setAttribute("readonly","readonly");
				request.setAttribute("open","");
				request.setAttribute("button","Reopen");
			}

			this.getServletContext().getRequestDispatcher("/Case.jsp").forward(request, response);
			
		} catch (final Exception ex) {
			ex.printStackTrace();
			this.getServletContext().getRequestDispatcher("/Cases.jsp").forward(request, response);
		}
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final HttpSession session = request.getSession(true);
		final User user = (User) session.getAttribute(UserManagement.SESSION_USER);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		if (request.getParameter("convEdited") != null) {
			int poiId = Integer.parseInt(request.getParameter("convEdited"));
			int caseNr = Integer.parseInt(request.getParameter("caseNr"));
			String reason = request.getParameter("reason");
			try {
				Date date = sdf.parse(request.getParameter("date"));
				String endDateString = request.getParameter("endDate");
				Date endDate = endDateString.isEmpty() ? null : sdf.parse(endDateString);
				Conviction c = new Conviction(date, endDate, reason, poiId, caseNr);
				dbInterface.updateConviction(c);
				doGet(request, response);
			} catch (ParseException ex) {
				ex.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			int caseNr;
			boolean open = false;
			
			caseNr = Integer.parseInt(request.getParameter("caseNr"));
			if("ON".equals(request.getParameter("open"))) {
				open = true;
			}
			
			String categoryName = request.getParameter("categoryName");
			if(categoryName != null && categoryName.equals("null")) {
				categoryName = null;
			}
			String title = request.getParameter("title");
			String location = request.getParameter("location");
			String description =  request.getParameter("description");
			String creator = request.getParameter("creator");
			String comment = request.getParameter("comment");
			String dateStr = request.getParameter("date");

			if(title.length() >= 255) {
				request.setAttribute("errorMsg", "Title must not be longer then 255 letters.");
				doGet(request, response);
				return;
			} else if(location.length() >= 255) {
				request.setAttribute("errorMsg", "Location must not be longer then 255 letters.");
				doGet(request, response);
				return;
			} else if(description.length() >= 255) {
				request.setAttribute("errorMsg", "Description must not be longer then 255 letters.");
				doGet(request, response);
				return;
			} else if(comment.length() >= 255) {
				request.setAttribute("errorMsg", "Comment must not be longer then 255 letters.");
				doGet(request, response);
				return;
			}
			
			Date date = null;
			try {
				date = sdf.parse(dateStr);
				if(date.after(new Date())) {
					request.setAttribute("errorMsg", "Date has to be in the past.");
					doGet(request, response);
					return;
				}
			} catch (ParseException ex) {
				request.setAttribute("errorMsg", "Date has to be in format yyyy-MM-dd.");
				doGet(request, response);
				return;
			}
			
			if (request.getParameter("savebutton") != null) {
				Case aCase = new Case(caseNr,categoryName,title,open,date,location,description,creator);
	
				this.dbInterface.updateCase(aCase);
	
				if (dbInterface.existCNote(caseNr,user.getUsername()))
					dbInterface.updateCNote(caseNr,user.getUsername(),comment);
				else
					dbInterface.insertCNote(caseNr,user.getUsername(),comment);
	
				request.setAttribute("state", "saved!");
				doGet(request, response);
			} else if (request.getParameter("closebutton") != null && open) {
				Case aCase = new Case(caseNr,categoryName,title,false,date,location,description,creator);
				dbInterface.updateCase(aCase);
				dbInterface.insertConvictionsForClosedCase(aCase);
				doGet(request, response);
			} else if (request.getParameter("closebutton") != null) {
				Case aCase = new Case(caseNr,categoryName,title,true,date,location,description,creator);
				dbInterface.updateCase(aCase);
				dbInterface.deleteConvictions(caseNr);
				doGet(request, response);
			} else {
				doGet(request,response);
			}
		}
	}
}