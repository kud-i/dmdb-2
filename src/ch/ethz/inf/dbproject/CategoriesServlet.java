package ch.ethz.inf.dbproject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.Category;
import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.util.CategoryManagement;
import ch.ethz.inf.dbproject.util.UserManagement;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

/**
 * Servlet implementation class of Case list page
 */
@WebServlet(description = "", urlPatterns = { "/Categories" })
public final class CategoriesServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface db = new DatastoreInterface();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CategoriesServlet() {
		super();
	}

	protected final void doPost(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {
		String superCategory = request.getParameter("super");
		if(superCategory.equals("null")) {
			superCategory = null;
		}
		db.insertCategory(new Category(request.getParameter("name"), superCategory));
		doGet(request,response);
	}

	protected final void doGet(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {

		final BeanTableHelper<Category> table = new BeanTableHelper<Category>(
				"category", "casesTable", Category.class);
		
		if(request.getParameter("action") != null) {
			db.deleteCategoryByName(request.getParameter("name"));
		}

		// Add columns to the new table
		table.addBeanColumn("Level", "level");
		table.addBeanColumn("Category", "name");
		table.addBeanColumn("Super Category", "superName");

		if(UserManagement.isLoggedIn(request.getSession(true))) {
			table.addLinkColumn("", "Delete", "Categories?action=delete&name=", "name");
		}

		table.addObjects(CategoryManagement.getCategories());
		request.setAttribute("table", table);

		// Finally, proceed to the Projects.jsp page which will render the
		// Projects
		this.getServletContext().getRequestDispatcher("/Categories.jsp")
				.forward(request, response);
	}
}