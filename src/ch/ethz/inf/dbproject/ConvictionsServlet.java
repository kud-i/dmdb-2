package ch.ethz.inf.dbproject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.Conviction;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

/**
 * Servlet implementation class of Case list page
 */
@WebServlet(description = "Convictions", urlPatterns = { "/Conviction" })
public final class ConvictionsServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = new DatastoreInterface();

	protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) 
			throws ServletException, IOException {
		final HttpSession session = request.getSession(true);

		/*******************************************************
		 * Construct a table to present all our results
		 *******************************************************/
		final BeanTableHelper<Conviction> table = new BeanTableHelper<Conviction>(
				"convictions" 		/* The table html id property */,
				"casesTable" /* The table html class property */,
				Conviction.class 	/* The class of the objects (rows) that will be displayed */
		);

		// Add columns to the new table
		table.addBeanColumn("POI ID", "poiId");
		table.addBeanColumn("Case Nr", "caseNr");
		table.addBeanColumn("Date", "date");
		table.addBeanColumn("End Date", "endDate");
		table.addBeanColumn("Reason", "reason");
		table.addLinkColumn(""	/* The header. We will leave it empty */,
				"View Case" 	/* What should be displayed in every row */,
				"Case?caseNr=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
				"caseNr" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
	
		final String poiId = request.getParameter("poiId");
		
		table.addObjects(this.dbInterface.getConvictionsByPoiId(Integer.parseInt(poiId)));
		
		// Pass the table to the session. This will allow the respective jsp page to display the table.
		session.setAttribute("convictions", table);


		// Finally, proceed to the Projects.jsp page which will render the Projects
		this.getServletContext().getRequestDispatcher("/Convictions.jsp").forward(request, response);
	}
}