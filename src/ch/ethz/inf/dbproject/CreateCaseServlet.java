package ch.ethz.inf.dbproject;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.Case;
import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.Poi;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

/**
 * Servlet implementation class CreateCase
 */
@WebServlet(description = "Adding Persons", urlPatterns = { "/CreateCase" })
public final class CreateCaseServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = new DatastoreInterface();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreateCaseServlet() {
		super();
	}

	/**
	 * @return 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String categoryName;
		String title;
		Date date = null; // Date auf null setzen
		String location;
		String description;
		String creator;
		String comment;

		categoryName = request.getParameter("category");
		title = request.getParameter("title"); // title parameter abfragen
		location = request.getParameter("location");
		description = request.getParameter("description");
		creator = request.getParameter("username");
		comment = request.getParameter("comment");

		String startDateStr = request.getParameter("date");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = sdf.parse(startDateStr);
		} catch (ParseException ex) {
			ex.printStackTrace();
		}

		Case aCase = new Case(0, categoryName, title, true, date, location,
				description, creator);
	
		int m = this.dbInterface.insertCase(aCase);
		this.dbInterface.insertCNote(m, creator, comment);

		String t = Integer.toString(m);
		request.setAttribute("caseNr", t);
		
		this.getServletContext().getRequestDispatcher("/Case?caseNr="+t).forward(request, response);
    }	
}
