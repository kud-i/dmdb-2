package ch.ethz.inf.dbproject;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.Poi;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

/**
 * Servlet implementation class Search
 */
@WebServlet(description = "Adding Persons", urlPatterns = { "/CreatePoi" })
public final class CreatePoiServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = new DatastoreInterface();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreatePoiServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/CreatePoi.jsp")
				.forward(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		final HttpSession session = request.getSession(true);

		final String lastName = request.getParameter("lastName");
		final String firstName = request.getParameter("firstName");
		final String birthdate = request.getParameter("birthdate");
		int poiId = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		Date now = null;
		if (birthdate.equals("")) {
			request.setAttribute("error",
					"Person of interest must have a birthdate!");
			doGet(request, response);
		} else {
			try {
				date = sdf.parse(birthdate);
			} catch (ParseException e1) {
				request.setAttribute("error",
						"Not a valid date!");
				doGet(request, response);
				return;
			}
			Calendar cal = Calendar.getInstance();
			now = cal.getTime();
			now.setYear(now.getYear() - 14);

			if (date.after(now)) {
				request.setAttribute("error",
						"Person of interest must be older then 14 years!");
				doGet(request, response);
			} else if (firstName.equals("")) {
				request.setAttribute("error",
						"Person of interest must have a firstname!");
				doGet(request, response);
			} else if (lastName.equals("")) {
				request.setAttribute("error",
						"Person of interest must have a lastname!");
				doGet(request, response);
			} else if (lastName.length() > 16) {
				request.setAttribute("error",
						"firstname can't be longer than 16 letters!");
				doGet(request, response);
			} else if (firstName.length() > 16) {
				request.setAttribute("error",
						"firstname can't be longer than 16 letters!");
				doGet(request, response);
			} else {
				try {
					poiId = this.dbInterface.insertPoi(firstName, lastName,	Long.toString(date.getTime()));
				} catch (SQLException e) {
					e.printStackTrace();
				}

				response.sendRedirect("Poi?poiId=" + poiId);
			}
		}
	}
}
