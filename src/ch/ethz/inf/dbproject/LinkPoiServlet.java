package ch.ethz.inf.dbproject;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.DatastoreInterface;

/**
 * Servlet implementation class LinkPoiServlet
 */
@WebServlet("/LinkPoi")
public class LinkPoiServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = new DatastoreInterface();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LinkPoiServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			final HttpSession session = request.getSession(true);

			final String nrString = request.getParameter("caseNr");
			final String poiString = request.getParameter("poiId");
			
			try {
				int caseNr = Integer.parseInt(nrString);
				int poiId = Integer.parseInt(poiString);
				
				this.dbInterface.insertInvolved(caseNr,poiId);
				
				
				
			} catch (final Exception ex) {
				ex.printStackTrace();
				this.getServletContext().getRequestDispatcher("/Case").forward(request, response);
			}

			this.getServletContext().getRequestDispatcher("/Case").forward(request, response);
		}

}
