package ch.ethz.inf.dbproject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.Comment;
import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.Poi;
import ch.ethz.inf.dbproject.model.User;
import ch.ethz.inf.dbproject.util.UserManagement;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

/**
 * Servlet implementation class of Case Details Page
 */
@WebServlet(description = "Displays a specific person of interest.", urlPatterns = { "/Poi" })
public final class PoiServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = new DatastoreInterface();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PoiServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected final void doGet(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {

		final HttpSession session = request.getSession(true);

		final String idString = request.getParameter("poiId");
		final User user = (User) session
				.getAttribute(UserManagement.SESSION_USER);

		if (idString == null) {
			this.getServletContext().getRequestDispatcher("/Pois")
					.forward(request, response);
			return;
		}

		try {

			final Integer id = Integer.parseInt(idString);
			final Poi aPoi = this.dbInterface.getPoiById(id);

			/*******************************************************
			 * Construct a table to present all properties of a case
			 *******************************************************/
			final BeanTableHelper<Comment> ctable = new BeanTableHelper<Comment>(
					"pois" /* The table html id property */,
					"casesTable" /* The table html class property */,
					Comment.class /*
								 * The class of the objects (rows) that will be
								 * displayed
								 */
			);

			// Add columns to the new table

			ctable.addBeanColumn("Note", "comment");
			ctable.addBeanColumn("User", "username");

			// TODO ohne akutellen Benutzer
			ctable.addObjects(this.dbInterface.getPNotesbyPoiId(aPoi.getPoiId()));
			// table.setVertical(true);

			if (user != null) {
				Comment c = dbInterface.getPNotebyPoiIdAndUser(aPoi.getPoiId(),
						user.getUsername());
				if (c != null) {
					session.setAttribute("note", c.getComment());
				} else
					session.setAttribute("note", "");
			}

			session.setAttribute("commentTable", ctable);

			session.setAttribute("poiId", aPoi.getPoiId());
			session.setAttribute("firstName", aPoi.getFirstName());
			session.setAttribute("lastName", aPoi.getLastName());
			session.setAttribute("birthdate", aPoi.getBirthdate());

			this.getServletContext()
					.getRequestDispatcher("/Poi.jsp?poiId=" + idString)
					.forward(request, response);
		} catch (final Exception ex) {
			ex.printStackTrace();
			this.getServletContext().getRequestDispatcher("/Pois.jsp")
					.forward(request, response);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final String idString = request.getParameter("poiId");
		final HttpSession session = request.getSession(true);
		final User user = (User) session
				.getAttribute(UserManagement.SESSION_USER);

		int poiId;
		boolean open = false;

		poiId = Integer.parseInt(request.getParameter("poiId"));

		if (request.getParameter("savebutton") != null) {

			String firstName;
			String lastName;
			Date birthdate = null; // birthdate auf null setzen
			String comment;

			firstName = request.getParameter("firstName");
			lastName = request.getParameter("lastName");
			comment = request.getParameter("comment");

			// Date
			String startDateStr = request.getParameter("birthdate");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			Date now = cal.getTime();
			now.setYear(now.getYear() - 14);
			if (startDateStr.equals("")) {
				request.setAttribute("error",
						"Person of interest must have a birthdate!");
				doGet(request, response);
			} else {

				try {
					birthdate = sdf.parse(startDateStr);
				} catch (ParseException e1) {
					request.setAttribute("error",
							"Not a valid date!");
					doGet(request, response);
					return;
				}
				cal = Calendar.getInstance();
				now = cal.getTime();
				now.setYear(now.getYear() - 14);

				if (birthdate.after(now)) {
					request.setAttribute("error", "Person of interest must be older then 14 years!");
					doGet(request, response);
				} else if (firstName.equals("")) {
					request.setAttribute("error", "Person of interest must have a firstname!");
					doGet(request, response);
				} else if (lastName.equals("")) {
					request.setAttribute("error",
							"Person of interest must have a lastname!");
					doGet(request, response);
				} else if (lastName.length()>16) { 
					request.setAttribute("error",
					"firstname can't be longer than 16 letters!");
					doGet(request,response); 
				} else if (firstName.length()>16){
					request.setAttribute("error",
					"firstname can't be longer than 16 letters!");
					doGet(request,response); }
				
				else {
					Poi aPoi = new Poi(poiId, firstName, lastName, birthdate);

					int b;

					b = this.dbInterface.updatePoi(aPoi);

					if (this.dbInterface.existPNote(poiId, user.getUsername()))
						this.dbInterface.updatePNote(poiId, user.getUsername(),
								comment);
					else
						this.dbInterface.insertPNote(poiId, user.getUsername(),
								comment);

					request.setAttribute("state", "saved!");
					doGet(request, response);
				}
			}
		}
	}
}