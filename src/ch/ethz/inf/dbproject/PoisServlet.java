package ch.ethz.inf.dbproject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.Case;
import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.Poi;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

@WebServlet(description = "Displays all persons of interest", urlPatterns = { "/Pois" })
public final class PoisServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = new DatastoreInterface();

	public PoisServlet() {
		super();
	}
	
	protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) 
			throws ServletException, IOException {

		final HttpSession session = request.getSession(true);

		/*******************************************************
		 * Construct a table to present all our results
		 *******************************************************/
		if (request.getParameter("addPerson") == null){
		final BeanTableHelper<Poi> table = new BeanTableHelper<Poi>(
				"cases" 		/* The table html id property */,
				"casesTable" /* The table html class property */,
				Poi.class 	/* The class of the objects (rows) that will bedisplayed */
		);
		
		// Add columns to the new table
		// Add columns to the new table

		/*
		 * Column 1: The name of the item (This will probably have to be changed)
		 */
		table.addBeanColumn("POI ID", "poiId");
		table.addBeanColumn("Firstname", "firstName");
		table.addBeanColumn("Lastname", "lastName");
		table.addBeanColumn("Birthdate", "birthdate");
		
		table.addLinkColumn(""	/* The header. We will leave it empty */,
				"View Person" 	/* What should be displayed in every row */,
				"Poi?poiId=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
				"poiId" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);

		table.addLinkColumn(""	/* The header. We will leave it empty */,
				"View Convictions" 	/* What should be displayed in every row */,
				"Conviction?poiId=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
				"poiId" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);

		table.addLinkColumn(""	/* The header. We will leave it empty */,
				"View Involved Cases" 	/* What should be displayed in every row */,
				"Cases?poiId=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
				"poiId" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);

		
		// Pass the table to the session. This will allow the respective jsp page to display the table.
		session.setAttribute("pois", table);

		table.addObjects(this.dbInterface.getAllPois());
		

		this.getServletContext().getRequestDispatcher("/Pois.jsp").forward(request, response);
		}
		else this.getServletContext().getRequestDispatcher("/CreatePoi.jsp").forward(request, response);
	}
}
