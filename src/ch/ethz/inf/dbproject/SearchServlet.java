package ch.ethz.inf.dbproject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.Case;
import ch.ethz.inf.dbproject.model.Conviction;
import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.Poi;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

/**
 * Servlet implementation class Search
 */
@WebServlet(description = "The search page for cases", urlPatterns = { "/Search" })
public final class SearchServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = new DatastoreInterface();
		
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final HttpSession session = request.getSession(true);

		/*******************************************************
		 * Construct a table to present all our search results
		 *******************************************************/
		final BeanTableHelper<Case> ctable = new BeanTableHelper<Case>(
				"cases" 		/* The table html id property */,
				"casesTable" /* The table html class property */,
				Case.class 	/* The class of the objects (rows) that will bedisplayed */
		);
		
		ctable.addBeanColumn("Case Nr.", "caseNr");
		ctable.addBeanColumn("Title", "title");
		ctable.addBeanColumn("Description", "description");
		ctable.addBeanColumn("Location", "location");
		ctable.addBeanColumn("Category", "categoryName");
		ctable.addBeanColumn("Date", "date");
		ctable.addBeanColumn("Creator", "username");
		ctable.addBeanColumn("Open", "open");
		
		ctable.addLinkColumn(""	/* The header. We will leave it empty */,
				"View Case" 	/* What should be displayed in every row */,
				"Case?caseNr=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
				"caseNr" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
		
		final BeanTableHelper<Poi> ptable = new BeanTableHelper<Poi>(
				"cases" 		/* The table html id property */,
				"casesTable" /* The table html class property */,
				Poi.class 	/* The class of the objects (rows) that will bedisplayed */
		);
		
		ptable.addBeanColumn("POI ID", "poiId");
		ptable.addBeanColumn("Firstname", "firstName");
		ptable.addBeanColumn("Lastname", "lastName");
		ptable.addBeanColumn("Birthdate", "birthdate");
		
		ptable.addLinkColumn(""	/* The header. We will leave it empty */,
				"View Person" 	/* What should be displayed in every row */,
				"Poi?poiId=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
				"poiId" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);

		ptable.addLinkColumn(""	/* The header. We will leave it empty */,
				"View Convictions" 	/* What should be displayed in every row */,
				"Conviction?poiId=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
				"poiId" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);

		ptable.addLinkColumn(""	/* The header. We will leave it empty */,
				"View Involved Cases" 	/* What should be displayed in every row */,
				"Cases?poiId=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
				"poiId" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);

		
		final BeanTableHelper<Conviction> contable = new BeanTableHelper<Conviction>(
				"cases" 		/* The table html id property */,
				"casesTable" /* The table html class property */,
				Conviction.class 	/* The class of the objects (rows) that will bedisplayed */
		);
		
		contable.addBeanColumn("POI ID", "poiId");
		contable.addBeanColumn("Case Nr", "caseNr");
		contable.addBeanColumn("Date", "date");
		contable.addBeanColumn("End Date", "endDate");
		contable.addBeanColumn("Reason", "reason");
		contable.addLinkColumn(""	/* The header. We will leave it empty */,
						"View Case" 	/* What should be displayed in every row */,
						"Case?caseNr=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
						"caseNr" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
		

		// The filter parameter defines what to show on the cases page
		final String filter = request.getParameter("filter");

		if (filter != null) {
		
			if(filter.equals("case")) {
				
				final String search = request.getParameter("case");
				ctable.addObjects(this.dbInterface.searchForCase(search));
				session.setAttribute("results", ctable);
				session.setAttribute("caset", search);

			} else if (filter.equals("poi")) {

				final String search = request.getParameter("poi");
				ptable.addObjects(this.dbInterface.searchForPoi(search));
				session.setAttribute("results", ptable);
				session.setAttribute("poit", search);

			} else if (filter.equals("conviction")) {

				final String search = request.getParameter("conviction");
				contable.addObjects(this.dbInterface.searchForConviction(search));	
				session.setAttribute("results", contable);
				session.setAttribute("cont", search);

			}			
		}else{
			session.setAttribute("results", "");
		}
		

		// Finally, proceed to the Seaech.jsp page which will render the search results
        this.getServletContext().getRequestDispatcher("/Search.jsp").forward(request, response);	        
	}
}