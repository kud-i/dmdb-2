package ch.ethz.inf.dbproject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.User;
import ch.ethz.inf.dbproject.util.UserManagement;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

@WebServlet(description = "Page that displays the user login / logout options.", urlPatterns = { "/User" })
public final class UserServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface db = new DatastoreInterface();

	public final static String SESSION_USER_LOGGED_IN = "userLoggedIn";
	public final static String SESSION_USER_DETAILS = "userDetails";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected final void doGet(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {
		final HttpSession session = request.getSession(true);

		final String username = request.getParameter("username");
		final String password = request.getParameter("password");
		if (request.getParameter("logout") != null) {
			request.setAttribute("login-error", "Logged out!");
			session.removeAttribute(UserManagement.SESSION_USER);
		} else if (request.getParameter("login") != null) {
			// Retrieve User
			// Store this user into the session
			User user = db.getUserByName(username);
			if(user == null) {
				request.setAttribute("login-error", "Wrong username!");
			} else if(user.getPassword().equals(password)) {
				session.setAttribute(UserManagement.SESSION_USER, user);
			} else {
				request.setAttribute("login-error", "Wrong password!");
			}
		} else if (request.getParameter("register") != null) {
			User user = db.getUserByName(username);
			if(user == null) {
				user = new User(username, password);
				db.insertUser(user);
			} else {
				request.setAttribute("register-error", "Username already given!");
			}
		}
		if(request.getAttribute("login-error") == null) {
			request.setAttribute("login-error", "");
		}
		if(request.getAttribute("register-error") == null) {
			request.setAttribute("register-error", "");
		}
		
		User loggedUser = UserManagement.getCurrentlyLoggedInUser(session);

		if (loggedUser == null) {
			// Not logged in!
			session.setAttribute(SESSION_USER_LOGGED_IN, false);
		}
		
		if(loggedUser != null) {
			// Logged in
			final BeanTableHelper<User> userDetails = new BeanTableHelper<User>("userDetails", "userDetails", User.class);
			userDetails.addBeanColumn("Username", "username");
			userDetails.addBeanColumn("Password", "password");
			
			userDetails.addObject(loggedUser);

			session.setAttribute(SESSION_USER_LOGGED_IN, true);
			session.setAttribute(SESSION_USER_DETAILS, userDetails);
		}

		// Finally, proceed to the User.jsp page which will render the profile
		this.getServletContext().getRequestDispatcher("/User.jsp").forward(request, response);
	}

}
