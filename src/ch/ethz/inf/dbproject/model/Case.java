package ch.ethz.inf.dbproject.model;

import java.util.Date;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

public final class Case {
	
	private int caseNr;
	private String categoryName;
	private String title;
	private boolean open;
	private Date date;
	private String location;
	private String description;
	private String username;
	
	/**
	 * Construct a new case.
	 * 
	 * @param description		The name of the case
	 */	
	public Case(int caseNr, String categoryName, String title, boolean open,
			Date date, String location, String description, String username) {
		this.caseNr = caseNr;
		this.categoryName = categoryName;
		this.title = title;
		this.open = open;
		this.date = date;
		this.location = location;
		this.description = description;
		this.username = username;
	}

	public Case(final Tuple rs) {
		// TODO Extra properties need to be added?
		this.caseNr = rs.getInt("caseNr");
		this.categoryName = rs.getString("categoryName");
		this.title = rs.getString("title");
		this.open = rs.getBoolean("open");
		this.date = rs.getDate("date");
		this.location = rs.getString("location");
		this.description = rs.getString("description");
		this.username = rs.getString("username");
		}
	/**
	 * HINT: In eclipse, use Alt + Shift + S menu and choose:
	 * "Generate Getters and Setters to auto-magically generate
	 * the getters. 
	 */

	public int getCaseNr() {
		return caseNr;
	}

	public void setCaseNr(int caseNr) {
		this.caseNr = caseNr;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean getOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}