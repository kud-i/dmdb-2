package ch.ethz.inf.dbproject.model;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

/**
 * Object that represents a category of project (i.e. Theft, Assault...) 
 */
public final class Category {
	/**
	 * TODO All properties need to be added here 
	 */	
	private final String name;
	private final String superName;
	private int level;

	public Category(final String name, final String superName) {
		super();
		this.name = name;
		this.superName = superName;
	}
	public Category(final Tuple rs) {
		this.name = rs.getString("name");
		this.superName = rs.getString("super");
	}

	public String getName() {
		return name;
	}

	public String getSuperName() {
		return superName;
	}

	public String getEncodedName() {
		try {
			return URLEncoder.encode(getName(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

	public int getLevel() {
		return level;
	}
	public void setLevel(int i) {
		this.level = i;
	}
}
