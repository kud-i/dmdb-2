package ch.ethz.inf.dbproject.model;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

/**
 * Object that represents a user comment.
 */
public class Comment {

	private final String username;
	private final String comment;
	
	public Comment(final String username, final String comment) {
		this.username = username;
		this.comment = comment;
	}

	public Comment(final Tuple rs) {
		this.username = rs.getString("username");
		this.comment = rs.getString("note");
	}
	
	public String getUsername() {
		return username;
	}

	public String getComment() {
		return comment;
	}	
}
