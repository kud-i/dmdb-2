package ch.ethz.inf.dbproject.model;

import java.util.Date;
import java.text.SimpleDateFormat;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

/**
 * Object that represents a conviction.
 */
public class Conviction {
	private final Date date;
	private final Date endDate;
	private final String reason;
	private final int poiId;
	private final int caseNr;

	public int getPoiId() {
		return poiId;
	}
	
	public int getCaseNr() {
		return caseNr;
	}
	
	public Date getDate() {
		return date;
	}

	public Date getEndDate() {
		return endDate;
	}
	
	public String getDateString() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return date == null ? "" : sdf.format(date);
	}
	
	public String getEndDateString() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return endDate == null ? "" : sdf.format(endDate);
	}

	public String getReason() {
		return reason;
	}
	
	public Conviction(final Date date, final Date endDate, final String reason, int poiId, int caseNr) {
		this.date = date;
		this.endDate = endDate;
		this.reason = reason;
		this.caseNr = caseNr;
		this.poiId = poiId;
	}
	
	public Conviction(Tuple rs) {
		this.date = rs.getDate("date");
		this.endDate = rs.getDate("endDate");
		this.reason = rs.getString("reason");
		this.poiId = rs.getInt("poiId");
		this.caseNr = rs.getInt("caseNr");
	}
}
