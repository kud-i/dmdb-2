package ch.ethz.inf.dbproject.model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ch.ethz.inf.dbproject.database.MySQLConnection;
import ch.ethz.inf.dbproject.model.simpleDatabase.operators.Operator;
import ch.ethz.inf.dbproject.model.simpleDatabase.sqlParser.Parser;
import ch.ethz.inf.dbproject.model.simpleDatabase.storage.TableManager;

/**
 * This class should be the interface between the web application AND the
 * database. Keeping all the data-access methods here will be very helpful for
 * part 2 of the project.
 */
public final class DatastoreInterface {

	public TableManager tmy;
	private Connection sqlConnection;
	private PreparedStatement pstmt;
	private Parser parser;
	
	//FIXME close bei update/insert!!

	public DatastoreInterface() {
		this.sqlConnection = MySQLConnection.getInstance().getConnection();
		tmy = new TableManager("db");
		this.parser = new Parser(tmy);
	}

	public final Case getCaseByNr(final int nr) {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("SELECT * FROM `Case` WHERE caseNr = ?");
			final Operator op;
			Case rCase = null;

			this.pstmt.setInt(1, nr);
			
			op = parser.parseSQL(this.pstmt.toString());
			
			if (op.moveNext())
				rCase = new Case(op.current());
			
			op.close();
			this.pstmt.close();

			return rCase;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}

	}

	public final List<Case> getAllCases() {
		try {

			this.pstmt = this.sqlConnection.prepareStatement("SELECT * FROM `Case`");
			final Operator rs;
			final List<Case> cases = new ArrayList<Case>();

			rs = parser.parseSQL(this.pstmt.toString());

			while (rs.moveNext()) {
				cases.add(new Case(rs.current()));
			}

			rs.close();
			this.pstmt.close();

			return cases;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public List<Case> getProjectsByCategory(String category) {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("SELECT * FROM `Case` WHERE categoryName = ?");
			final Operator rs;
			final List<Case> cases = new ArrayList<Case>();

			this.pstmt.setString(1,category);
			rs = parser.parseSQL(this.pstmt.toString());
			
			while (rs.moveNext()) {
				cases.add(new Case(rs.current()));
			}


			rs.close();
			this.pstmt.close();

			return cases;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}

	}
	
	public final List<Poi> getAllPois() {
		try {
			this.pstmt = this.sqlConnection.prepareStatement("SELECT * FROM Poi");
			final Operator rs;
			final List<Poi> pois = new ArrayList<Poi>();

			rs = parser.parseSQL(this.pstmt.toString());

			while (rs.moveNext()) {
				pois.add(new Poi(rs.current()));
			}

			rs.close();
			this.pstmt.close();

			return pois;
			
		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public final List<Poi> getNotLinkedPoisbyCaseNr(int caseNr) {
		try {
			this.pstmt = this.sqlConnection.prepareStatement("SELECT * FROM Poi WHERE poiId NOT IN (SELECT poiId FROM Involved WHERE caseNr = ?)");
				//old	"SELECT * FROM Poi P, Involved I  WHERE P.poiId NOT IN (SELECT poiId FROM Involved WHERE caseNr = ?)");
			final Operator rs;
			final List<Poi> pois = new ArrayList<Poi>();
			
			this.pstmt.setInt(1, caseNr);

			rs = parser.parseSQL(this.pstmt.toString());

			while (rs.moveNext()) {
				pois.add(new Poi(rs.current()));
			}

			rs.close();
			this.pstmt.close();

			return pois;
			
		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}
	

	public List<Case> getCasesbyStatus(boolean open) {
		
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("SELECT * FROM `Case` WHERE open = ?");
			final Operator rs;
			final List<Case> cases = new ArrayList<Case>();

			String open1 = "false";
			if (open) open1 = "true";
			this.pstmt.setString(1, open1);
			rs = parser.parseSQL(this.pstmt.toString());
			
			while (rs.moveNext()) {
				cases.add(new Case(rs.current()));
			}


			rs.close();
			this.pstmt.close();

			return cases;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	private List<Conviction> getConvictionsBy(PreparedStatement prepared) throws SQLException {
		pstmt = prepared;
		final Operator rs;
		final List<Conviction> convictions = new ArrayList<Conviction>();

		rs = parser.parseSQL(this.pstmt.toString());
		while (rs.moveNext()) {
			convictions.add(new Conviction(rs.current()));
		}

		rs.close();
		rs.close();
			this.pstmt.close();

		return convictions;
	}

	public Conviction getConviction(int caseNr, int poiId) {
		try {
			PreparedStatement s = sqlConnection.prepareStatement("SELECT * FROM Conviction WHERE caseNr = ? AND poiId = ?");
			s.setInt(1, caseNr);
			s.setInt(2, poiId);
			List<Conviction> convs = getConvictionsBy(s);
			return convs.isEmpty() ? null : convs.get(0);
		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public List<Conviction> getConvictionsByCaseNr(int caseNr) {
		try {
			PreparedStatement s = sqlConnection.prepareStatement("SELECT * FROM Conviction WHERE caseNr = ?");
			s.setInt(1, caseNr);
			return getConvictionsBy(s);
		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public final List<Conviction> getConvictionsByPoiId(int poiId) {
		try {
			PreparedStatement s = sqlConnection.prepareStatement("SELECT * FROM Conviction WHERE poiId = ?");
			s.setInt(1, poiId);
			return getConvictionsBy(s);
		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public List<Case> getMostRecentCases() {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("SELECT * FROM `Case` ORDER BY `date` DESC LIMIT 10;");
			final Operator rs;
			final List<Case> cases = new ArrayList<Case>();

			rs = parser.parseSQL(this.pstmt.toString());
			
			while (rs.moveNext()) {
				cases.add(new Case(rs.current()));
			}


			rs.close();
			this.pstmt.close();

			return cases;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public final Poi getPoiById(final int id) {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("SELECT * FROM Poi WHERE poiId = ?");
			final Operator rs;
			final Poi aPoi;

			this.pstmt.setInt(1, id);
			rs = parser.parseSQL(this.pstmt.toString());
			rs.moveNext();
			aPoi = new Poi(rs.current());
			
			rs.close();
			this.pstmt.close();
			
			return aPoi;
		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public List<Case> getOldestUnsolvedCases() {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("SELECT * FROM `Case` WHERE open = TRUE ORDER BY `date` LIMIT 10;");
			final Operator rs;
			final List<Case> cases = new ArrayList<Case>();

			rs = parser.parseSQL(this.pstmt.toString());

			while (rs.moveNext()) {
				cases.add(new Case(rs.current()));
			}

			rs.close();
			this.pstmt.close();

			return cases;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public final List<Case> getCasesByPoiId(int poiId) {
		try {
			this.pstmt = this.sqlConnection.prepareStatement("SELECT* FROM `Case` C, Involved I WHERE C.caseNr = I.caseNr AND I.poiId = ?");
			final Operator rs;
			final List<Case> cases = new ArrayList<Case>();

			this.pstmt.setInt(1, poiId);
			
			rs = parser.parseSQL(this.pstmt.toString());
			
			while (rs.moveNext()) {
				cases.add(new Case(rs.current()));
			}
			
			rs.close();
			this.pstmt.close();
			
			return cases;
		
		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public List<Case> getCasesByCreator(String username) {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement(
					"SELECT * FROM `Case` WHERE username = ?");
			final Operator rs;
			final List<Case> cases = new ArrayList<Case>();

			this.pstmt.setString(1, username);
			rs = parser.parseSQL(this.pstmt.toString());
			while (rs.moveNext()) {
				cases.add(new Case(rs.current()));
			}
			return cases;
		
		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public Comment getPNotebyPoiIdAndUser(int poiId, String username) {
		try {

			this.pstmt = this.sqlConnection.prepareStatement("SELECT * FROM `PNote` WHERE poiId = ? AND username = ?");
			final Operator rs;
			final Comment comment;

			this.pstmt.setInt(1, poiId);
			this.pstmt.setString(2, username);
			
			rs = parser.parseSQL(this.pstmt.toString());

			if (rs.moveNext())
				comment = new Comment(rs.current());
			else
				comment = null;
			
			rs.close();
			this.pstmt.close();

			return comment;
		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public int updateCase(Case aCase) {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("UPDATE `Case` SET categoryName = ?," +
					"title = ?,`open` = ?,`date` = ?,location = ?,description = ?," +
					"username = ? WHERE caseNr = ?;");
			final Operator rs;

			this.pstmt.setString(1, aCase.getCategoryName() );
			this.pstmt.setString(2,aCase.getTitle()  );
			String open = "false";
			if (aCase.getOpen()) open = "true";
			this.pstmt.setString(3, open );
		
			String date = Long.toString(aCase.getDate().getTime());
			this.pstmt.setString(4, date);
			
			this.pstmt.setString(5, aCase.getLocation() );
			this.pstmt.setString(6, aCase.getDescription() );
			this.pstmt.setString(7, aCase.getUsername() );
			this.pstmt.setInt(8,  aCase.getCaseNr());
			
			rs = parser.parseSQL(this.pstmt.toString());
			
			while(rs.moveNext()){
				//FIXME
				//append result id
			}
			
			
			
			rs.close();
			this.pstmt.close();

			return 0;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return 0;
		}
	}

	public int updatePoi(Poi aPoi) {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("UPDATE `Poi` SET firstName = ?," +
					"lastName = ?, birthdate = ? WHERE poiId = ?");
			final Operator rs;

			this.pstmt.setString(1, aPoi.getFirstName() );
			this.pstmt.setString(2,aPoi.getLastName()  );
		
			String date = Long.toString(aPoi.getBirthdate().getTime());
			this.pstmt.setString(3, date);
			
			this.pstmt.setInt(4, aPoi.getPoiId());
	
			rs = parser.parseSQL(this.pstmt.toString());
			
			while(rs.moveNext()){
				//FIXME
				//append result id
			}
			
			
			rs.close();
			this.pstmt.close();

			return 0;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return 0;
		}
	}

	public int insertConvictionsForClosedCase(Case aCase) {
		try {
			this.pstmt = this.sqlConnection.prepareStatement(
					"INSERT INTO Conviction (poiId, caseNr, `date`)" +
					"SELECT poiId,caseNr,CURDATE()'' FROM Involved WHERE caseNr = ?");
			final Operator rs;

			this.pstmt.setInt(1, aCase.getCaseNr());

			rs = parser.parseSQL(this.pstmt.toString());
			
			int result = 0;
			while(rs.moveNext()) {
				result = rs.current().getInt(0);
			}
			rs.close();
			this.pstmt.close();

			return result;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return 0;
		}
	}

	public int deleteConvictions(int caseNr) {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("DELETE FROM Conviction WHERE caseNr = ?");
			final Operator rs;

			this.pstmt.setInt(1, caseNr);

			rs = parser.parseSQL(this.pstmt.toString());
			
			while(rs.moveNext()){
				//FIXME
				//append result id
			}

			rs.close();
			this.pstmt.close();

			return 0;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return 0;
		}
	}

	public List<Comment> getCNotesbyCaseNr(int caseNr) {
		try {

			this.pstmt = this.sqlConnection.prepareStatement("SELECT * FROM `CNote` WHERE caseNr = ? AND note <> ''");
			final Operator rs;
			final List<Comment> comments = new ArrayList<Comment>();

			this.pstmt.setInt(1, caseNr);
			rs = parser.parseSQL(this.pstmt.toString());

			while (rs.moveNext()) {
				comments.add(new Comment(rs.current()));
			}

			rs.close();
			this.pstmt.close();

			return comments;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public Comment getCNotebyCaseNrAndUser(int caseNr, String username) {
		try {

			this.pstmt = this.sqlConnection.prepareStatement("SELECT * FROM `CNote` WHERE caseNr = ? AND username = ?");
			final Operator rs;
			final Comment comment;

			this.pstmt.setInt(1, caseNr);
			this.pstmt.setString(2, username);
			rs = parser.parseSQL(this.pstmt.toString());

			comment = rs.moveNext() ? new Comment(rs.current()) : null;
			
			rs.close();
			this.pstmt.close();

			return comment;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public int updateCNote(int caseNr, String username, String comment) {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("UPDATE CNote SET note = ? WHERE caseNr = ? AND username =?");
			final Operator rs;

			this.pstmt.setString(1, comment );
			this.pstmt.setInt(2,caseNr );
			this.pstmt.setString(3, username );
		
			rs = parser.parseSQL(this.pstmt.toString());
			
			while(rs.moveNext()){
				//FIXME
				//append result id
			}
			
			rs.close();
			this.pstmt.close();

			return 0;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return 0;
		}
	}

	public boolean existCNote(int caseNr, String username) {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("SELECT * FROM CNote WHERE caseNr = ? AND username = ?");
			final Operator rs;
			boolean b;

			this.pstmt.setInt(1,caseNr) ;
			this.pstmt.setString(2, username );
		
			rs = parser.parseSQL(this.pstmt.toString());

			b = rs.moveNext();
			
			rs.close();
			this.pstmt.close();

			return (b);

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return false;
		}
	}

	public int insertCNote(int caseNr, String username, String comment) {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("INSERT INTO CNote (caseNr, username, note) VALUES (?,?,?)");
			final Operator rs;

			this.pstmt.setInt(1,caseNr );
			this.pstmt.setString(2, username );
			this.pstmt.setString(3, comment );
				
			rs = parser.parseSQL(this.pstmt.toString());
			
			while(rs.moveNext()){
				//FIXME
				//append result id
			}
			
			rs.close();
			this.pstmt.close();

			return 0;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return 0;
		}
		
	}

	public final int insertPoi(String firstName, String lastName, String birthdate) throws SQLException {
		this.pstmt = this.sqlConnection.prepareStatement("INSERT INTO Poi (firstName,lastName,birthdate) VALUES (?,?,?)",this.pstmt.RETURN_GENERATED_KEYS);
		
		final Operator rs;
		final int result;
		
		this.pstmt.setString(1, firstName);
		this.pstmt.setString(2, lastName);
		this.pstmt.setString(3, birthdate);

		rs = parser.parseSQL(this.pstmt.toString());
		rs.moveNext();
		result = rs.current().getInt(0);
		rs.close();
		
		return result;
	}
	
	private java.sql.Date javaToSqlDate(java.util.Date date) {
		return date == null ? null : new java.sql.Date(date.getTime());
	}

	public void updateConviction(Conviction con) throws SQLException {
		this.pstmt = this.sqlConnection.prepareStatement("REPLACE Conviction (endDate, `date`, reason,poiId,caseNr) VALUES (?,?,?,?,?)");
		java.sql.Date date = javaToSqlDate(con.getDate());
		java.sql.Date endDate = javaToSqlDate(con.getEndDate());

		String date1 = Long.toString(endDate.getTime());
		this.pstmt.setString(1, date1);
		String date2 = Long.toString(date.getTime());
		this.pstmt.setString(2, date2);
		pstmt.setString(3, con.getReason());
		pstmt.setInt(4, con.getPoiId());
		pstmt.setInt(5, con.getCaseNr());

		Operator rs = parser.parseSQL(this.pstmt.toString());
		while(rs.moveNext()) {}
		rs.close();
	}

	public int insertCase(Case aCase) {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("INSERT INTO `Case` " +
					"(`categoryName`,`title`,`open`,`date`,`location`,`description`,`username`)" +
					" VALUES (?,?,?,?,?,?,?)",this.pstmt.RETURN_GENERATED_KEYS);
			final Operator rs;
			final int result;

			this.pstmt.setString(1, aCase.getCategoryName() );
			this.pstmt.setString(2,aCase.getTitle()  );
			String open = "false";
			if (aCase.getOpen()) open = "true";
			this.pstmt.setString(3, open );

			String date = Long.toString(aCase.getDate().getTime());
			this.pstmt.setString(4, date);//FIXME maybe

			this.pstmt.setString(5, aCase.getLocation() );
			this.pstmt.setString(6, aCase.getDescription() );
			this.pstmt.setString(7, aCase.getUsername() );

			//FIXME
			rs = parser.parseSQL(this.pstmt.toString());
			rs.moveNext();
			result = rs.current().getInt(0);
			rs.close();

			this.pstmt.close();

			return result;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return 0;
		}
    }

	
	public List<Comment> getPNotesbyPoiId(int poiId) {
		try {
			this.pstmt = this.sqlConnection.prepareStatement("SELECT * FROM `PNote` WHERE poiId = ? AND note <> ''");
			final Operator rs;
			final List<Comment> comments = new ArrayList<Comment>();

			this.pstmt.setInt(1, poiId);
			rs = parser.parseSQL(this.pstmt.toString());

			while (rs.moveNext()) {
				comments.add(new Comment(rs.current()));
			}

			rs.close();
			this.pstmt.close();

			return comments;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public int insertPNote(int poiId, String username, String comment) {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("INSERT INTO PNote (poiId, username, note) VALUES  (?,?,?)");
			
			final Operator rs;

			this.pstmt.setInt(1, poiId );
			this.pstmt.setString(2, username );
			this.pstmt.setString(3, comment );
				
			rs = parser.parseSQL(this.pstmt.toString());
			
			//FIXME Add to INSERT/UPDATE
			while (rs.moveNext()) {
				System.out.println(rs.current().toString());
				System.out.println("Insert");
				//get Insert Status...
				//rs.current().get())
			}
			//FIXME Add to ev.
			rs.close();
			this.pstmt.close();

			return 0;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return 0;
		}
		
	}
	
	public int updatePNote(int poiId, String username, String comment) {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("UPDATE PNote SET note = ? WHERE poiId = ? AND username =?");
			final Operator rs;

			this.pstmt.setString(1, comment );
			this.pstmt.setInt(2, poiId );
			this.pstmt.setString(3, username );
		
			rs = parser.parseSQL(this.pstmt.toString());
			
			while(rs.moveNext()){
				//FIXME
				//append result id
			}
			
			rs.close();
			this.pstmt.close();

			return 0;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return 0;
		}
	}

	public boolean existPNote(int poiId, String username) {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("SELECT * FROM PNote WHERE poiId = ? AND username =?");
			final Operator rs;
			boolean b = false;

			this.pstmt.setInt(1,poiId) ;
			this.pstmt.setString(2, username );
		
			rs = parser.parseSQL(this.pstmt.toString());
			
			b = rs.moveNext();

			rs.close();
			this.pstmt.close();

			return (b);
		} catch (final SQLException ex) {
			ex.printStackTrace();
			return false;
		}
	}

	public List<Poi> getPoiByCaseNr(int caseNr) {
		try {
			this.pstmt = this.sqlConnection.prepareStatement("SELECT * FROM Poi P, Involved I WHERE P.poiId = I.poiId AND I.caseNr = ?");
			final Operator rs;
			final List<Poi> pois = new ArrayList<Poi>();

			this.pstmt.setInt(1, caseNr);
			
			rs = parser.parseSQL(this.pstmt.toString());

			while (rs.moveNext()) {
				pois.add(new Poi(rs.current()));
			}
			
			rs.close();
			this.pstmt.close();
			
			return pois;
			
		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public List<Category> getRootCategories() {
		return getSubCategories(null);
	}

	public List<Category> getSubCategories(Category parent) {
		try {
			if(parent == null) {
				this.pstmt = this.sqlConnection.prepareStatement(
						"SELECT `name`,`super` FROM `Category` " +
						"WHERE `super` IS NULL ORDER BY `name`");
			} else {
				this.pstmt = this.sqlConnection.prepareStatement(
						"SELECT `name`,`super` FROM `Category` " +
						"WHERE `super` = ? ORDER BY `name`");
				this.pstmt.setString(1, parent.getName());
			}

			Operator rs = parser.parseSQL(this.pstmt.toString());
			List<Category> categories = new ArrayList<Category>();
	
			while (rs.moveNext()) {
				categories.add(new Category(rs.current()));
			}
	
			rs.close();
			this.pstmt.close();
			return categories;
		} catch (final SQLException ex) {
			ex.printStackTrace();
			return Collections.emptyList();
		}
	}

	public void insertCategory(Category category) {
		try {
			if(category.getSuperName() != null) {
				pstmt = sqlConnection.prepareStatement("INSERT INTO `Category` " +
						"(`name`,`super`) VALUES (?,?)",
						Statement.RETURN_GENERATED_KEYS);
				pstmt.setString(1, category.getName());
				pstmt.setString(2, category.getSuperName());
			} else {
				pstmt = sqlConnection.prepareStatement("INSERT INTO `Category` " +
						"(`name`) VALUES (?)",
						Statement.RETURN_GENERATED_KEYS);
				pstmt.setString(1, category.getName());
			}
			Operator rs = parser.parseSQL(this.pstmt.toString());
			while(rs.moveNext());
			rs.close();
			pstmt.close();
		} catch (final SQLException ex) {
			ex.printStackTrace();
		}
	}

	public int insertInvolved(int caseNr, int poiId) {
		try {
		this.pstmt  = this.sqlConnection.prepareStatement("INSERT INTO `Involved`(`poiId`,`caseNr`) VALUES(?,?)");
		final Operator rs;

		this.pstmt.setInt(1, poiId );
		this.pstmt.setInt(2, caseNr );
		
		
		rs = parser.parseSQL(this.pstmt.toString());
			
			while(rs.moveNext()){
				//FIXME
				//append result id
			}
		
		rs.close();
			this.pstmt.close();

		return 0;

	} catch (final SQLException ex) {
		ex.printStackTrace();
		return 0;
	}
		
	}

	public int deleteInvolved(int caseNr, int poiId) {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("DELETE FROM Involved WHERE poiId = ? AND caseNr = ?");
			final Operator rs;

			this.pstmt.setInt(1, poiId );
			this.pstmt.setInt(2, caseNr );
			
			
			rs = parser.parseSQL(this.pstmt.toString());
			
			while(rs.moveNext()){
				//FIXME
				//append result id
			}
			
			rs.close();
			this.pstmt.close();

			return 0;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return 0;
		}
		
	}
	
	public int deleteCategoryByName(String name) {
		try {
			final Operator rs;
			pstmt = sqlConnection.prepareStatement("DELETE FROM `Category` " +
					" WHERE `name` = ?", Statement.RETURN_GENERATED_KEYS);
	
			pstmt.setString(1, name);

			rs = parser.parseSQL(this.pstmt.toString());
			
			while (rs.moveNext()) {
				System.out.println(rs.current().toString());
				System.out.println("Insert");
				//FIXME get Return Keys...
				//rs.current().get())
			}
			
			rs.close();
			pstmt.close();
			
			return 0;
			
		} catch (final SQLException ex) {
			ex.printStackTrace();
			return ex.getErrorCode();
		}
	}
	
	public User getUserByName(String username) {
		try {
			this.pstmt = this.sqlConnection.prepareStatement(
					"SELECT `username`,`password` FROM `User` " +
					"WHERE `username` = ?");
			this.pstmt.setString(1, username);

			Operator rs = parser.parseSQL(this.pstmt.toString());
			User user = rs.moveNext() ? new User(rs.current()) : null;
			rs.close();
			this.pstmt.close();
			return user;
		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public int insertUser(User user) {
		try {
			this.pstmt  = this.sqlConnection.prepareStatement("INSERT INTO `User` " +
					"(`username`,`password`)" +
					" VALUES (?,?)", Statement.RETURN_GENERATED_KEYS);
			final int result;

			this.pstmt.setString(1, user.getUsername());
			this.pstmt.setString(2, user.getPassword());
			
			final Operator rs = parser.parseSQL(this.pstmt.toString());//this.pstmt.getGeneratedKeys();
			rs.moveNext();
			result = rs.current().getInt(0);
			rs.close();

			this.pstmt.close();

			return result;
		} catch (final SQLException ex) {
			ex.printStackTrace();
			return 0;
		}
	}

	public List<Case> searchForCase(String search) {
		try {

			this.pstmt = this.sqlConnection.prepareStatement("SELECT * FROM `Case` WHERE `caseNr` LIKE ? OR " +
					"`categoryName` LIKE ? OR `title` LIKE ? OR `open` LIKE ? OR `date`LIKE ? OR " +
					"`location`LIKE ? OR `description`LIKE  ? OR `username` LIKE ?");
			final Operator rs;
			final List<Case> cases = new ArrayList<Case>();
			
			search = ".*" + search + ".*";
			//search = "%" + search + "%";
			
			this.pstmt.setString(1, search);
			this.pstmt.setString(2, search);
			this.pstmt.setString(3, search);
			if (search.equalsIgnoreCase(".*true.*"))
				this.pstmt.setString(4, "tru");
			 else if (search.equalsIgnoreCase(".*false.*"))
				this.pstmt.setString(4, "false");
			else
				this.pstmt.setString(4, search);
			String date;
			try {
				String[] s = search.split("-");
				Date date1 = new Date(Integer.parseInt(s[0].substring(2))-1900,Integer.parseInt(s[1])-1,Integer.parseInt(s[2].substring(0,s[2].length()-2)));
				date = Long.toString(date1.getTime());
				date = date.substring(0, date.length()-8) + ".*";
			}
			catch (Exception e){
				date="";
			}
			this.pstmt.setString(5, date);
			this.pstmt.setString(6, search);
			this.pstmt.setString(7, search);
			this.pstmt.setString(8, search);

			rs = parser.parseSQL(this.pstmt.toString());

			while (rs.moveNext()) {
				cases.add(new Case(rs.current()));
			}

			rs.close();
			this.pstmt.close();

			return cases;

		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public List<Poi> searchForPoi(String search) {
		try {
			this.pstmt = this.sqlConnection.prepareStatement("SELECT * FROM `Poi` WHERE `poiId` LIKE ? OR " +
					"`lastName` LIKE ? OR `firstName` LIKE ? OR `birthdate`LIKE ?");
			final Operator rs;
			final List<Poi> pois = new ArrayList<Poi>();
			
			//search = "%" + search + "%";
			search = ".*" + search + ".*";
			
			this.pstmt.setString(1, search);
			this.pstmt.setString(2, search);
			this.pstmt.setString(3, search);
			String date;
			try {
				String[] s = search.split("-");
				Date date1 = new Date(Integer.parseInt(s[0].substring(2))-1900,Integer.parseInt(s[1])-1,Integer.parseInt(s[2].substring(0,s[2].length()-2)));
				date = Long.toString(date1.getTime());
				date = date.substring(0, date.length()-8) + ".*";
			}
			catch (Exception e){
				date="";
			}
			this.pstmt.setString(4, date);
			
			rs = parser.parseSQL(this.pstmt.toString());

			while (rs.moveNext()) {
				pois.add(new Poi(rs.current()));
			}

			rs.close();
			this.pstmt.close();

			return pois;
			
		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public List<Conviction> searchForConviction(String search) {
		try {

			this.pstmt = this.sqlConnection.prepareStatement("SELECT * FROM `Conviction` WHERE `endDate` LIKE ? OR " +
					"`date` LIKE ? OR `reason` LIKE ? OR `poiId` LIKE ? OR `caseNr` LIKE ?");
			final Operator rs;
			final List<Conviction> cons = new ArrayList<Conviction>();

			//search = "%" + search + "%";
			search = ".*" + search + ".*";
			
			String date;
			try {
				String[] s = search.split("-");
				Date date1 = new Date(Integer.parseInt(s[0].substring(2))-1900,Integer.parseInt(s[1])-1,Integer.parseInt(s[2].substring(0,s[2].length()-2)));
				date = Long.toString(date1.getTime());
				date = date.substring(0, date.length()-8) + ".*";
			}
			catch (Exception e){
				date="";
			}
			this.pstmt.setString(1, date);
			this.pstmt.setString(2, date);
			this.pstmt.setString(3, search);
			this.pstmt.setString(4, search);
			this.pstmt.setString(5, search);
			
			rs = parser.parseSQL(this.pstmt.toString());

			while (rs.moveNext()) {
				cons.add(new Conviction(rs.current()));
			}

			rs.close();
			this.pstmt.close();

			return cons;
		} catch (final SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}
}


