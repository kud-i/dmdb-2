package ch.ethz.inf.dbproject.model;
import java.util.Date;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

public final class Poi {
	
	private final int poiId;
	private final String firstName;
	private final String lastName;
	private final Date birthdate;

	public Poi(final int poiId,final String firstName,final String lastName,final Date birthdate) {
		this.poiId = poiId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthdate = birthdate;
	}

	public Poi(final Tuple rs) {
		this.poiId = rs.getInt("poiId");
		this.firstName = rs.getString("firstName");
		this.lastName = rs.getString("lastName");
		this.birthdate = rs.getDate("birthdate");
	}

	public int getPoiId() {
		return poiId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	
}

