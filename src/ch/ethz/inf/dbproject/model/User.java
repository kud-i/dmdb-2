package ch.ethz.inf.dbproject.model;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

/**
 * Object that represents a registered in user.
 */
public final class User {
	private final String username;
	private final String password;
	
	public User(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public User(final Tuple rs) {
		this.username = rs.getString("username");
		this.password = rs.getString("password");
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
}
