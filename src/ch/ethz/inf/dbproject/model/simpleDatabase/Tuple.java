package ch.ethz.inf.dbproject.model.simpleDatabase;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

/**
 * A tuple in our database. A tuple consists of a schema (describing the names
 * of the columns) and values. A tuple is created and modified by operators.
 * 
 * You can use String to store the values. In case you need a specific type,
 * you can use the additional getType methods.
 */
public class Tuple {

	private final TupleSchema schema;
	private final TupleIdentifier identifier;
	
	private final String[] values;
	private final byte[] bytes;

	public Tuple(TupleSchema schema, String[] values) {
		this(schema, values, null);
	}

	public Tuple(TupleSchema schema, String[] values, TupleIdentifier ti) {
		this.schema = schema;
		this.values = values;
		this.identifier = ti;
		this.bytes = toBytes();
	}

	public Tuple(TupleSchema schema, String[] values, byte[] bytes, int pageNr, int slotNr) {
		this.schema = schema;
		this.values = values;
		this.bytes = bytes;
		this.identifier = new TupleIdentifier(pageNr, slotNr);
	}
	
	public Tuple withIdentifier(TupleIdentifier ti) {
		return new Tuple(schema, values, ti);
	}
	
	public TupleIdentifier getIdentifier() {
		return identifier;
	}

	public final TupleSchema getSchema() {
		return this.schema;
	}
	
	public final String get(String col) {
		return get(schema.getIndex(col));
	}

	public final String get(final int index) {
		return this.values[index];
	}

	/*public final short getShort(final int index) {
		return Short.parseShort(this.values[index]);
	}
	
	public final Date getDate(final int index) {
		return new Date(getLong(index));
	}
	
	public final int getInt(final int index) {
		return Integer.parseInt(this.values[index]);
	}

	public final long getLong(final int index) {
		return Long.parseLong(this.values[index]);
	}
	
	public final float getFloat(final int index) {
		return Float.parseFloat(this.values[index]);
	}
	
	public final double getDouble(final int index) {
		return Double.parseDouble(this.values[index]);
	}*/

	public final String toString() {
		final StringBuilder buf = new StringBuilder();
		for (int i = 0; i < values.length; i++) {
			buf.append(values[i]);
			if (i < values.length - 1) {
				buf.append(",");
			}
		}
		return buf.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((schema == null) ? 0 : schema.hashCode());
		result = prime * result + Arrays.hashCode(values);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tuple other = (Tuple) obj;
		if (schema == null) {
			if (other.schema != null)
				return false;
		} else if (!schema.equals(other.schema))
			return false;
		if (!Arrays.equals(values, other.values))
			return false;
		return true;
	}

	public String[] getValues(){
		return values;
	}

	private final byte[] toBytes() {
		final StringBuilder buf = new StringBuilder();
		for (int i = 0; i < values.length; i++) {
			if(i != 0) {
				buf.append('\0');
			}
			buf.append(values[i]);
		}
		return buf.toString().getBytes();
	}
	
	public byte[] getBytes() {
		return bytes;
	}

	public int getPageNr() {
		return identifier.getPageNr();
	}

	public int getSlotNr() {
		return identifier.getSlotNr();
	}
	
	public String[] getValues(String[] keys) {
		String[] subvalues = new String[keys.length];
		for(int i = 0; i < keys.length; i++) {
			subvalues[i] = this.get(keys[i]);
		}
		return subvalues;
	}
	
	public String[] getValues(List<String> keys) {
		String[] subvalues = new String[keys.size()];
		for(int i = 0; i < keys.size(); i++) {
			subvalues[i] = this.get(keys.get(i));
		}
		return subvalues;
	}
	
	public String getValueString(String[] keys) {
		StringBuffer buff = new StringBuffer();
		for(int i = 0; i < keys.length; i++) {
			buff.append(this.get(keys[i]));
			buff.append('\t');
		}
		return buff.toString();
	}

	public int getIndexByString(String s){
		return this.getSchema().getIndex(s);
	}
	
	public int getInt(String string) {
		int i = 0;
		try {
		i = Integer.parseInt(this.get(this.getIndexByString(string)));
		}
		catch (Exception e) {
		i = 0;
		}
		return i;
	}

	public String getString(String string) {
		return this.get(this.getIndexByString(string));
	}

	public boolean getBoolean(String string) {
		String s = this.get(this.getIndexByString(string));
		boolean result = false;
		if (s.equals("true")){
			result = true;
		}
		return result;
	}

	public java.util.Date getDate(String string) {
		try{
		long milliSecs = Long.parseLong(this.getString(string));
		return new Date(milliSecs);
		}
		catch (Exception e){
			return new Date(0);
		}
	}

	public int getInt(int i) {
		int k = 0;
		try {
		k = Integer.parseInt(this.get(i));
		}
		catch (Exception e) {
		k = 0;
		}
		return k;
	}
}
