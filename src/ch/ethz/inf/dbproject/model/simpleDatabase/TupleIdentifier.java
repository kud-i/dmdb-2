package ch.ethz.inf.dbproject.model.simpleDatabase;

public class TupleIdentifier {
	private final int pageNr, slotNr;
	
	public TupleIdentifier(int pageNr, int slotNr) {
		this.pageNr = pageNr;
		this.slotNr = slotNr;
	}
	
	public int getPageNr() {
		return pageNr;
	}

	public int getSlotNr() {
		return slotNr;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + pageNr;
		result = prime * result + slotNr;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TupleIdentifier other = (TupleIdentifier) obj;
		if (pageNr != other.pageNr)
			return false;
		if (slotNr != other.slotNr)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TID(" + pageNr + "," + slotNr + ")";
	}
}
