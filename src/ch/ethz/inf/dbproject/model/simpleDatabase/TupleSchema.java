package ch.ethz.inf.dbproject.model.simpleDatabase;

import java.util.Arrays;
import java.util.HashMap;

/**
 * The schema contains meta data about a tuple. So far we only store the name of
 * each column. Other meta data, such cardinalities, indexes, etc. could be
 * specified here.
 */
public class TupleSchema {
	private final String[] primaryKey;
	private final String[] columnNames;
	private final String[] autoIncrement;
	private final HashMap<String, Integer> columnNamesMap;
	
	public TupleSchema(String[] columnNames) {
		this(columnNames, new String[0], new String[0]);
	}

	public TupleSchema(
		final String[] columnNames,
		final String[] primaryKey,
		final String[] autoIncrement
	) {
		this.columnNames = columnNames;
		this.primaryKey = primaryKey;
		this.autoIncrement = autoIncrement;
		
		this.columnNamesMap = new HashMap<String, Integer>();
		for (int i = 0; i < columnNames.length; ++i) {
			this.columnNamesMap.put(this.columnNames[i].toUpperCase(), i);
		}
	}

	/**
	 * Given the name of a column, returns the index in the respective tuple.
	 * 
	 * @param column column name
	 * @return index of column in tuple
	 */
	public int getIndex(final String column) {
		final Integer index = this.columnNamesMap.get(column.toUpperCase());
		if (index == null) {
			return -1; // error
		} else {
			return index;
		}
	}
	
	public String[] getColumns(){
		return columnNames;
	}

	public String[] getPrimaryKey() {
		return primaryKey;
	}

	public String[] getAutoIncrement() {
		return autoIncrement;
	}

	public Tuple parse(byte[] b, int o, int len, int pageNr, int slotNr) {
		String[] values = new String[columnNames.length];
		byte[] bytes = new byte[len];
		int i = o;
		for(int v = 0; v < values.length; v++) {
			int j;
			for(j = i; b[j] != 0 && j < o + len; j++) {
				bytes[j-o] = b[j];
			}
			values[v] = new String(b, i, j-i);
			i = j + 1;
		}
		return new Tuple(this, values, bytes, pageNr, slotNr);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(columnNames);
		result = prime * result
				+ ((columnNamesMap == null) ? 0 : columnNamesMap.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TupleSchema other = (TupleSchema) obj;
		if (!Arrays.equals(columnNames, other.columnNames))
			return false;
		if (columnNamesMap == null) {
			if (other.columnNamesMap != null)
				return false;
		} else if (!columnNamesMap.equals(other.columnNamesMap))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TupleSchema [primaryKey=" + Arrays.toString(primaryKey)
				+ ", columnNames=" + Arrays.toString(columnNames)
				+ ", autoIncrement=" + Arrays.toString(autoIncrement)
				+ ", columnNamesMap=" + columnNamesMap + "]";
	}
}
