package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

public enum AggregateFuncs {
	COUNT, SUM, AVERAGE;
}
