package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.util.List;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

public class BufferedOp extends Operator {
	
	private final List<Tuple> buffer;
	private int count;
	
	public BufferedOp (List<Tuple> buffer){
		this.buffer = buffer;
		count = 0;
	}

	@Override
	public boolean moveNext() {
		if (count < buffer.size()){
			this.current = buffer.get(count);
			count++;
			return true;
		}
		return false;
	}

}
