package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.io.IOException;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleSchema;
import ch.ethz.inf.dbproject.model.simpleDatabase.storage.*;

public class Delete extends Operator {
	private static final TupleSchema RS = new TupleSchema(new String [] {
			"pageNr", "slotNr"
	});
	private final PageManager into;
	private final Operator source;

	/**
	 * This will try to delete all tuples the source gives us (in the form of pageNr, slotNr)
	 * from the PageManager, returning a Tuple of (pageNr, slotNr) from the deleted values.
	 */
	public Delete(final Operator source, final PageManager into) {
		this.into = into;
		this.source = source;
	}

	@Override
	public boolean moveNext() {
		if(source.moveNext()) {
			try {
				Tuple t = source.current();
				if(t.getPageNr() < 0 || t.getSlotNr() < 0) {
					current = new Tuple(RS, new String[] {
							Integer.toString(t.getPageNr()), Integer.toString(t.getSlotNr()) });
				} else {
					Page p = into.load(t.getPageNr(), true);
					p.delete(t.getSlotNr());
					current = new Tuple(RS, new String[] {
							Integer.toString(p.pageNr), Integer.toString(t.getSlotNr()) });
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}
	
	@Override
	public void close() {
		try {
			into.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
