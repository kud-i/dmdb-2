package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ch.ethz.inf.dbproject.model.simpleDatabase.JoinPair;
import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleSchema;

public class GroupBy extends Operator{
	
	private Join joinOp;
	private final Operator op;
	private ArrayList<Tuple> buffer;
	private final String[] columns;
	private final AggregateFuncs[] aggregateFuncs;
	private int[] index;
	private final String[] aggregateArgs;// If no Aggregation enter any 
	private final TupleSchema newSchema;
	
	public GroupBy (final Operator op, final String[] columns, final AggregateFuncs[] aggregateFuncs, final String[] aggregateArgs){
		this.op = op;
		this.index = new int[aggregateArgs.length];
		this.buffer = new ArrayList<Tuple>();
		this.columns = columns;
		this.aggregateFuncs = aggregateFuncs;
		this.aggregateArgs = aggregateArgs;
		String[] t = new String[columns.length+aggregateArgs.length];
		int i = 0;
		for (String c : columns){
			t[i]=c;
			i++;
		}
		int start = i;
		for (String c : aggregateArgs){
			String s = "";
			switch (aggregateFuncs[i-start]){
				case COUNT:
					s = "count_of_";
					break;
				case AVERAGE:
					s = "average_of_";
					break;
				case SUM:
					s = "sum_of_";
					break;
			}
			t[i]=c+s;
			i++;
		}
		this.newSchema = new TupleSchema(t);
		/*if (Arrays.equals(aggregateFuncs,new AggregateFuncs[0])) {
			ArrayList<JoinPair<String>> join = new ArrayList<JoinPair<String>>();
			for (int k = 0;k < columns.length;k++){
				join.add(new JoinPair<String>(columns[k], columns[k]));
				while (this.op.moveNext()) { 
					this.buffer.add(this.op.current());
				}
				joinOp = new Join(new BufferedOp(this.buffer), new BufferedOp(this.buffer), join);
			}
		}*/
	}
	
	
	
	
	private int offset;
	
	@Override
	public boolean moveNext() {
		/*if (Arrays.equals(aggregateFuncs,new AggregateFuncs[0])){
			if (joinOp.moveNext()){
				this.current = joinOp.current;
				return true;
			}
			else return false;
		}*/
		
		// a) if this is the first call:
		if (offset == 0) {
			//   1) fetch _all_ tuples from this.op and store them in group buffer
			while (this.op.moveNext()) { 
				this.buffer.add(this.op.current());
			}
			//   2) group the buffer
			List<Group> groupList = toList(buffer);
			if (groupList.isEmpty()) return false;
			buffer = new ArrayList<Tuple>();
			if (Arrays.equals(aggregateFuncs,new AggregateFuncs[0])) {
				for (Group current : groupList){
					String[] t = new String[groupList.get(0).values.length];
					int i = 0;
					for (String c : current.values){
						t[i]=c;
						i++;
					}
					buffer.add(new Tuple(newSchema,t));
				}
			}
			else {
			AggregateFuncs aggregateFunc;
			for (Group current : groupList){
				String[] t = new String[groupList.get(0).values.length+aggregateArgs.length];
				int i = 0;
				for (String c : current.values){
					t[i]=c;
					i++;
				}
			for (int e = 0;e < aggregateFuncs.length;e++){
				String result = "";
				aggregateFunc = aggregateFuncs[e];
				switch (aggregateFunc) {
				case COUNT:
						result = Integer.toString(current.args[e].size());
					break;
					
				case SUM:
						int sum = 0;
						for (String k  : current.args[e]){
							try{
								sum += Integer.parseInt(k);
							}
							catch (Exception e1){
							}
						}
						result = Integer.toString(sum);
					break;
					
				case AVERAGE:
						sum = 0;
						for (String k  : current.args[e]){
							try{
								sum += Integer.parseInt(k);
							}
							catch (Exception e1){

							}
						}
						int avg = sum/current.args[e].size();
						result = Integer.toString(avg);
					break;

					/*buffer = new ArrayList<Tuple>();
					for (Group current : groupList){
						if (current.args.size())
					}*/
			}
			t[i+e] = result;
			}
			buffer.add(new Tuple(newSchema,t));
			}
			}
			//   3) set the current tuple to the first one in the buffer and 
			//      remember you are at offset 0
			if (this.buffer.size() > offset){
				this.current = this.buffer.get(offset);
				offset +=1;
				return true;
			}else{
				return false;
			}
		// b) if this is not the first call 
		}else if (this.buffer.size() > offset){
			//   1) increase the offset and if it is valid fetch the next tuple
			this.current = this.buffer.get(offset);
			offset +=1;
			return true;
		}

		return false;
	//}
	}


private List<Group> toList(ArrayList<Tuple> buffer2) {
		List<Group> result = new ArrayList<Group>();
		Group current;
		boolean first = true;
		for (Tuple t : buffer2){
			if (first){
				for (int e = 0; e < aggregateArgs.length;e++)
				index[e] = t.getIndexByString(aggregateArgs[e]);
				first = false;
			}
			String[] oldValues = t.getValues();
			String[] oldColumns = t.getSchema().getColumns();
			String[] newValues = new String[columns.length];
			int columnsCount = 0;
			int valueCount = 0;
			
			for (String new_ : columns){
				valueCount = 0;
			for (String old : oldColumns){
				
				
					if (old.equals(new_)){
						newValues[columnsCount] = oldValues[valueCount];
						columnsCount++;
					}
					valueCount++;
				}
				
			}
			int length = aggregateArgs.length;
			String[] args = new String[length];
			for (int e = 0;e < length;e++){
				args[e] = t.get(index[e]);
			}
			Group currentGroup = new Group(newValues,args);
			boolean exists = false;
			for (Group g : result){
				if (Arrays.equals(g.values,currentGroup.values)){
					for (int e = 0;e < length;e++){
						g.args[e].add(currentGroup.args[e].get(0));
					}
					
					exists = true;
				}
			}
			if (!exists){
				result.add(currentGroup);
			}
		}
		return result;
	}

class Group {
	public String[] values;
	public List<String>[] args;
	public Group(String[] values, String[] args){
		this.values = values;
		int length = args.length;
		List<String>[] t = new List[length];
		for (int i = 0;i < length;i++){
			t[i] = new ArrayList<String>();
			t[i].add(args[i]);
		}
		this.args = t;
	}
}
}
