
package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.io.IOException;

import ch.ethz.inf.dbproject.model.simpleDatabase.TupleIdentifier;
import ch.ethz.inf.dbproject.model.simpleDatabase.storage.PageManager;

public class IndexSelect extends Operator {
	private final PageManager page;
	private final String[] values;
	
	private boolean finished = false;

	public IndexSelect(
		final PageManager page, 
		final String[] values
	) {
		this.page = page;
		this.values = values;
	}

	@Override
	public boolean moveNext() {
		if(finished) {
			this.current = null;
			return false;
		}
		finished = true;
		TupleIdentifier tid = page.getPrimaryKey().get(values);
		if(tid == null) {
			current = null;
			return false;
		} else {
			try {
				current = page.load(tid.getPageNr(), true).getSlot(tid.getSlotNr());
			} catch (IOException e) {
				current = null;
				e.printStackTrace();
				return false;
			}
			return true;
		}
	}
}
