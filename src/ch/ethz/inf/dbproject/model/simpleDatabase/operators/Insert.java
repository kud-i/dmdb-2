package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.io.IOException;
import java.util.Arrays;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleSchema;
import ch.ethz.inf.dbproject.model.simpleDatabase.storage.*;

public class Insert extends Operator {
	private static final TupleSchema ERR = new TupleSchema(new String [] {});
	private final PageManager into;
	private final Operator source;
	
	private final TupleSchema schema;
	private final TupleSchema insertSchema;

	public Insert(final Operator source, final PageManager into) {
		this(source, into, into.schema);
	}
	
	public Insert(final Operator source, final PageManager into, TupleSchema insertSchema) {
		this.into = into;
		this.source = source;
		String[] incs = into.schema.getAutoIncrement();
		String[] columns = Arrays.copyOf(incs, incs.length + 2);
		columns[incs.length] = "pageNr";
		columns[incs.length+1] = "slotNr";
		this.schema = new TupleSchema(columns);
		this.insertSchema = insertSchema;
	}

	@Override
	public boolean moveNext() {
		if(source.moveNext()) {
			try {
				Tuple t = source.current();
				if(insertSchema.getColumns().length == t.getSchema().getColumns().length) {
					t = new Tuple(insertSchema, t.getValues());
					Page p = into.getInsertPage(t);
					Tuple it = p.prepareAndAppend(t);
					String[] keys = schema.getColumns();
					String[] columns = new String[keys.length];
					for(int i = 0; i < columns.length - 2; i++) {
						columns[i] = it.get(keys[i]);
					}
					columns[columns.length - 2] = Integer.toString(it.getPageNr());
					columns[columns.length - 1] = Integer.toString(it.getSlotNr());
					current = new Tuple(schema, columns);
				} else {
					current = new Tuple(ERR, new String[] {});
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (AlreadyExistException e) {
				e.printStackTrace();
				current = new Tuple(ERR, new String[] {});
			}
			return true;
		}
		return false;
	}

	@Override
	public void close() {
		try {
			into.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
