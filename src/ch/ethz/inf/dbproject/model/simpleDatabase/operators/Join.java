package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ch.ethz.inf.dbproject.model.simpleDatabase.JoinPair;
import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleSchema;

public class Join extends Operator {

	private final Operator opL;
	private final Operator opR;
	private final ArrayList<JoinPair<String>> joinColumns;
	private final ArrayList<Tuple> bufferL;
	private final ArrayList<Tuple> bufferR;
	private ArrayList<Tuple> resultBuffer;
	
	public Join(
			final Operator opL,
			final Operator opR,
			final ArrayList<JoinPair<String>> joinColumns
		) {
			this.opL = opL;
			this.opR = opR;
			this.joinColumns = joinColumns;
			this.bufferL = new ArrayList<Tuple>();
			this.bufferR = new ArrayList<Tuple>();
			this.resultBuffer = new ArrayList<Tuple>();
			this.offset = 0;
		}
	
	private final ArrayList<Tuple> nestedLoopJoin(ArrayList<Tuple> left,ArrayList<Tuple> right,ArrayList<JoinPair<String>> jColumns) {
		ArrayList<Tuple> res = new ArrayList<Tuple>();
		ArrayList<JoinPair<Integer>> joinIndicies = new ArrayList<>();

		if (left.size() > 0 && right.size() > 0){

			//Assumption schema is equal for all tuples in same array list 
			TupleSchema schemaL = left.get(0).getSchema();
			TupleSchema schemaR = right.get(0).getSchema();
			for(JoinPair<String> cn: jColumns){
				int lIndex =  schemaL.getIndex(cn.getLeft());
				int rIndex =  schemaR.getIndex(cn.getRight());
				if (lIndex >= 0 && rIndex >= 0)
					joinIndicies.add(new JoinPair<Integer>(lIndex,rIndex));
			}
			

			//Outer Loop
			for(Tuple l: left){
				//Inner Loop
				for (Tuple r: right){
					//Check all Join Conditions
					boolean b = false;
					for (JoinPair<Integer> i: joinIndicies) {
						if (!l.get(i.getLeft()).equals(r.get(i.getRight()))) {
							b = false;
							break;
						} else {
							b = true;
						}
					}
					//if all conditions hold
					if (b)
						res.add(leftUnionOnJoinColumns(l,r,joinColumns));
					
				}
			}
		}
		return res;
	}
	
	static Tuple leftUnionOnJoinColumns (Tuple l, Tuple r, List<JoinPair<String>> jcolumns ){
		List<String> schemaL = new ArrayList<String>(Arrays.asList(l.getSchema().getColumns()));
		List<String> valuesL = new ArrayList<String>(Arrays.asList(l.getValues()));
		String[] schemaR = r.getSchema().getColumns();
		String[] valuesR = r.getValues();
		
		//get all right join columns
		List<String> rjColumns = new ArrayList<String>();
		for(JoinPair<String> p: jcolumns){
			rjColumns.add(p.getLeft());			
		}
		
		//go through right schema
		int index = 0;
		for(String sr: schemaR){
			if (!rjColumns.contains(sr)){
				schemaL.add(sr);
				valuesL.add(valuesR[index]);
			}
			index+=1;
		}
		
		String[] array = new String[schemaL.size()];
		TupleSchema ts = new TupleSchema(schemaL.toArray(array));

		array = new String[valuesL.size()];
		valuesL.toArray(array);
		return new Tuple(ts, valuesL.toArray(array));
	}
	
	private int offset;	
	@Override
	public boolean moveNext() {
		if (offset == 0) {
			//   1) fetch _all_ tuples from this.opL and this.opR and store them in buffers
			while (this.opL.moveNext()) { 
				this.bufferL.add(this.opL.current());
			}
			//FIXME Load Second Table op by op
			while (this.opR.moveNext()) { 
				this.bufferR.add(this.opR.current());
			}
			//   2) join the buffers and store result in resultBuffer
			this.resultBuffer = nestedLoopJoin(this.bufferL, this.bufferR, this.joinColumns);
	
			//   3) set the current tuple to the first one in the join buffer and 
			//      remember you are at offset 0
			if (this.resultBuffer.size() > offset){
				this.current = this.resultBuffer.get(offset);
				offset +=1;
				return true;
			}else{
				return false;
			}
		// b) if this is not the first call 
		}else if (this.resultBuffer.size() > offset){
			//   1) increase the offset and if it is valid fetch the next tuple
			this.current = this.resultBuffer.get(offset);
			offset +=1;
			return true;
		}

		return false;
	}

	@Override
	public String toString() {
		return "JOIN " + opL + " WITH " + opR + " ON " + joinColumns + " BY NESTED LOOP";
	}
}

