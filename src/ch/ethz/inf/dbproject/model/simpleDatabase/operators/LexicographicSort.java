package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.util.Comparator;
import java.util.List;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

public class LexicographicSort implements Comparator<Tuple> {
	private final List<Comparator<Tuple>> comparators;
	
	public LexicographicSort(List<Comparator<Tuple>> comparators) {
		this.comparators = comparators;
	}

	@Override
	public int compare(Tuple o1, Tuple o2) {
		int result = 0;
		// Lexicographic comparison
		for(Comparator<Tuple> comp: comparators) {
			result = comp.compare(o1, o2);
			if(result != 0) {
				break;
			}
		}
		return result;
	}

	@Override
	public String toString() {
		return comparators.toString();
	}
}
