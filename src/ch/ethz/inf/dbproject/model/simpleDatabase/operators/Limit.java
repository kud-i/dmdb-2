package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

public class Limit extends Operator {

	final int limit;
	final Operator op;
	private int count;
	
	public Limit(
			final Operator op, 
			final int limit
		) {
			this.op = op;
			this.limit = limit;
			this.count = 0;
		}
	
	@Override
	public boolean moveNext() {
		if (count < limit && this.op.moveNext()){
			this.current = this.op.current;
			count++;
			return true;
		}
		else return false;
	}

}
