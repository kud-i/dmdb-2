package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.util.ArrayList;
import java.util.List;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

public class NotIn extends Operator {

	private final Operator opL;
	private final Operator opR;
	private final List<Tuple> bufferR = new ArrayList<Tuple>();
	private final String argL;
	private final String argR;
	
	public NotIn (Operator opL, Operator opR, String argL, String argR){
		this.opL = opL;
		this.opR = opR;
		this.argL = argL;
		this.argR = argR;
		while (opR.moveNext()){
			this.bufferR.add(this.opR.current());
		}
	}
	
	@Override
	public boolean moveNext() {
		if (opL.moveNext()){
			Tuple t = opL.current;
			if (!isIn(t,bufferR)){
				this.current = t;
				return true;
			}
			else {
				while (opL.moveNext()){
					t = opL.current;
					if (!isIn(t,bufferR)){
						this.current = t;
						return true;
					}
				}
			}
		}
		return false;
	}

	private boolean isIn(Tuple t, List<Tuple> bufferR2) {
		for (Tuple t2 : bufferR2){
			if (t.getString(argL).equals(t2.getString(argR))){
				return true;
			}
		}
		return false;
	}

}
