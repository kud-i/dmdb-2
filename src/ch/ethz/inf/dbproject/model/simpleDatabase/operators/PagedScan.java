package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.io.IOException;

import ch.ethz.inf.dbproject.model.simpleDatabase.storage.*;

/**
 * The scan operator reads tuples from a file. The lines in the file contain the
 * values of a tuple. The line a comma separated.
 */
public class PagedScan extends Operator {
	private final PageManager pages;
	private Page page;
	private int pageNr = -1;
	private int slotNr = 0;

	/**
	 * Constructs a new scan operator (mainly for testing purposes).
	 * @param reader reader to read lines from
	 * @param columns column names
	 */
	public PagedScan(final PageManager pages) {
		this.pages = pages;
	}

	@Override
	public boolean moveNext() {
		try {
			if(pageNr < 0) {
				pageNr = 0;
				page = pages.load(0, false);
			}
			// Have to possible iterate through everything, since everything
			// could be deleted and we want to forward to first non deleted tuple :)
			while(page != null) {
				for(; slotNr <= page.lastSlot(); slotNr++) {
					current = page.getSlot(slotNr);
					if(current != null && page.notForwarded(slotNr)) {
						slotNr += 1;
						return true;
					}
				}
				pageNr += 1;
				slotNr = 0;
				page = pages.load(pageNr, false);
			}
			current = null;
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}


	@Override
	public String toString() {
		return "SCAN " + pages.getTableName();
	}
}
