package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.util.Arrays;
import java.util.Date;
import java.util.Random;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleSchema;

/**
 * Projection in relational algebra. Returns tuples that contain on projected
 * columns. Therefore the new tuples conform to a new schema.
 */
public final class Project extends Operator {
	private static Random rand = new Random();
	private static enum ProjectFuncs {
		CURDATE {
			String value() {
				return Long.toString(new Date().getTime());
			}
		}, RANDINT {
			String value() {
				return Integer.toString(rand.nextInt());
			}
		}, RANDLONG {
			String value() {
				return Long.toString(rand.nextLong());
			}
		}, ZERO { // For testing, you know.
			String value() {
				return "0";
			}
		};
		abstract String value();
	}

	private final Operator op;
	private final String[] newColumns;
	private final TupleSchema newSchema;

	/**
	 * Constructs a new projection operator.
	 * @param op operator to pull from
	 * @param column single column name that will be projected
	 */
	public Project(Operator op, String newColumn) {
		this(op, new String[] { newColumn });
	}

	public Project(
		final Operator op, 
		final String[] newColumns
	) {
		this.op = op;
		this.newColumns = newColumns;
		this.newSchema = new TupleSchema(newColumns);
	}

	@Override
	public boolean moveNext() {
		boolean itr = false;
		if (this.op.moveNext()) {
			String[] oldValues = this.op.current.getValues();
			String[] oldColumns = this.op.current.getSchema().getColumns();
			String[] newValues = new String[newColumns.length];
			int columnsCount = 0;
			int valueCount = 0;
			
			for (String new_ : newColumns) {
				if(new_.endsWith("()")) {
					newValues[columnsCount] = ProjectFuncs.valueOf(new_.substring(0,new_.length() - 2)).value();
					columnsCount += 1;
				} else {
					valueCount = 0;
					for (String old : oldColumns) {
						if (old.equals(new_)){
							newValues[columnsCount] = oldValues[valueCount];
							columnsCount += 1;
						}
						valueCount++;
					}
				}
				
			}
		this.current = new Tuple(newSchema, newValues);
		itr = true;
		}
		return itr;
	}

	@Override
	public String toString() {
		return "PROJECT " + Arrays.toString(newColumns) + " FROM " + op;
	}
}
