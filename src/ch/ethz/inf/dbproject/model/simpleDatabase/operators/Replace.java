package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.io.IOException;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleIdentifier;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleSchema;
import ch.ethz.inf.dbproject.model.simpleDatabase.storage.*;

public class Replace extends Operator {
	private static final TupleSchema RS = new TupleSchema(new String [] {
			"pageNr", "slotNr"
	});
	private final PageManager into;
	private final Operator source;

	/**
	 * This will try to insert all tuples the source gives us into the PageManager,
	 * returning a Tuple
	 */
	public Replace(final Operator source, final PageManager into) {
		this.into = into;
		this.source = source;
	}

	@Override
	public boolean moveNext() {
		if(source.moveNext()) {
			try {
				Tuple t = source.current();
				Index idx = into.getPrimaryKey();
				TupleIdentifier tid = idx.get(t.getValues(idx.getKeys()));
				Page p = into.load(tid.getPageNr(), true);
				p.update(tid.getSlotNr(), t);
				current = new Tuple(RS, new String[] {
						Integer.toString(tid.getPageNr()), Integer.toString(tid.getSlotNr()) });
			} catch (IOException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}
	
	@Override
	public void close() {
		try {
			into.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
