
package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.io.StringReader;
import java.util.Arrays;
import java.util.Scanner;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleSchema;


/**
 * The scan operator reads tuples from a file. The lines in the file contain the
 * values of a tuple. The line a comma separated.
 */
public class Scan extends Operator {
	private final TupleSchema schema;
	private final Scanner scanner;
	
	public Scan(
		final String s,
		final String[] columns
	) {
		this(s, new TupleSchema(columns));
	}

	public Scan(
		final String s,
		final TupleSchema schema
	) {
		this.schema = schema;
		this.scanner = new Scanner(new StringReader(s.substring(1,s.length() - 1)));
		this.scanner.useDelimiter("\\),\\(");
	}

	@Override
	public boolean moveNext() {
		// a) read next line
		// b) check if we are at the end of the file (line would be null)
		//   b1. if we reached end of the file, close the buffered reader
		// c) split up comma separated values
		// d) create new tuple using schema and values
		if(scanner.hasNext()) {
			String line = scanner.next();
			
			String[] values = Arrays.copyOf(line.split(","), schema.getColumns().length);
			for(int i = 0; i < values.length; i++) {
				if(values[i] == null) {
					values[i] = "";
				}
			}
			current = new Tuple(schema, values);
			return true;
		} else { // No leaking of memory, bro!
			current = null;
		}
		return false;
	}

}
