
package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

public class Select extends Operator {
	private final Operator op;
	private final String column;
	private final String compareValue;
	private final SelectorFunc func;

	public Select(
		final Operator op, 
		final String column, 
		final String compareValue, 
		final SelectorFunc func
	) {
		this.op = op;
		this.column = column;
		this.compareValue = compareValue;
		this.func = func;
	}

	@Override
	public boolean moveNext() {
		while(this.op.moveNext()) {
			final Tuple t = this.op.current();
			if(func.match(t.get(column), this.compareValue)) {
				this.current = t;
				return true;
			}
		}
		this.current = null;
		return false;
	}

	@Override
	public String toString() {
		return "SELECT (" + op + ") WHERE " + column + " " + func + " " + compareValue;
	}
}
