package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

public class SelectLikeOr extends Operator {
	private final Operator op;
	private final String[] columns;
	private final String[] compareValues;
	private final SelectorFunc func;
	
	public SelectLikeOr(
		final Operator op, 
		final String[] columns, 
		final String[] compareValues 
	) {
		this.op = op;
		this.columns = columns;
		this.compareValues = compareValues;
		this.func = SelectorFuncs.LIKE;
	}

	@Override
	public boolean moveNext() {
		while(this.op.moveNext()) {
			for (int i = 0;i < columns.length;i++){
				final Tuple t = this.op.current();
				if(func.match(t.get(columns[i]), this.compareValues[i])) {
					this.current = t;
					return true;
				}
			}
		}
		this.current = null;
		return false;
	}

}
