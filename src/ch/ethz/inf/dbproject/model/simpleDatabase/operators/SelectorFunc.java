package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

public interface SelectorFunc {
	boolean match(String db, String query);
}
