package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.util.regex.Pattern;

public enum SelectorFuncs implements SelectorFunc {
	EQUAL {
		@Override
		public boolean match(String db, String query) {
			return db.equals(query);
		}
	}, NOT_EQUAL {
		@Override
		public boolean match(String db, String query) {
			return !EQUAL.match(db,query);
		}
	}, LIKE {
		@Override
		public boolean match(String db, String query) {
			Pattern p = Pattern.compile(query, Pattern.CASE_INSENSITIVE);
			return p.matcher(db).matches();
		}
	};
}
