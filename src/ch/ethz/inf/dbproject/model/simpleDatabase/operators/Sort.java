package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

/**
 * An empty sort operator
 * For the purposes of the project, you can consider only string comparisons of
 * the values.
 */
public class Sort extends Operator implements Comparator<Tuple> {
	private final Operator op;
	private final Comparator<Tuple> comparator;
	private final ArrayList<Tuple> sortBuffer;

	public Sort(
		final Operator op,
		final String column,
		final boolean ascending
	) {
		this(op, new SortFunc(column, ascending));
	}

	public Sort(
		final Operator op,
		final Comparator<Tuple> comparator
	) {
		this.op = op;
		this.comparator = comparator;
		this.sortBuffer = new ArrayList<Tuple>();
		this.offset = 0;
	}
	
	@Override
	public final int compare(final Tuple l, final Tuple r) {
		int result = 0;

		return result;
	}

	private int offset;
	
	@Override
	public boolean moveNext() {
		// a) if this is the first call:
		if (offset == 0) {
			//   1) fetch _all_ tuples from this.op and store them in sort buffer
			while (this.op.moveNext()) { 
				this.sortBuffer.add(this.op.current());
			}
			//   2) sort the buffer
			Collections.sort(this.sortBuffer, comparator);
			//   3) set the current tuple to the first one in the sort buffer and 
			//      remember you are at offset 0
			if (this.sortBuffer.size() > offset){
				this.current = this.sortBuffer.get(offset);
				offset +=1;
				return true;
			}else{
				return false;
			}
		// b) if this is not the first call 
		} else if (this.sortBuffer.size() > offset){
			//   1) increase the offset and if it is valid fetch the next tuple
			this.current = this.sortBuffer.get(offset);
			offset +=1;
			return true;
		}

		return false;
	}

	@Override
	public String toString() {
		return "ORDER (" + op + ") BY " + comparator;
	}

}
