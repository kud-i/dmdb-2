package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.util.Comparator;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

public class SortFunc implements Comparator<Tuple> {
	private String column;
	private boolean asc;
	
	public SortFunc(String column, boolean asc) {
		this.column = column;
		this.asc = asc;
	}

	@Override
	public int compare(Tuple o1, Tuple o2) {
		int res = o1.get(column).compareTo(o2.get(column));
		return asc ? res : -res;
	}
	
	@Override
	public String toString() {
		return (asc ? "+" : "-") + column;
	}
}
