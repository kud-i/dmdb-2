package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.util.*;

import ch.ethz.inf.dbproject.model.simpleDatabase.JoinPair;
import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

public class SortMerge extends Operator {
	private final Operator opL;
	private final Operator opR;
	private final List<JoinPair<String>> joinColumns;
	private final List<String> lcols, rcols;
	
	private SortMergeStatus s = SortMergeStatus.INIT;
	
	private Stack<Tuple> leftBuffer;
	private List<Tuple> rightBuffer;
	private int rightIndex;

	public SortMerge(Operator opL, Operator opR,
			ArrayList<JoinPair<String>> joinColumns) {
		lcols = new ArrayList<String>();
		rcols = new ArrayList<String>();
		List<Comparator<Tuple>> lcomps = new LinkedList<Comparator<Tuple>>();
		List<Comparator<Tuple>> rcomps = new LinkedList<Comparator<Tuple>>();
		for(JoinPair<String> pair: joinColumns) {
			lcols.add(pair.getLeft());
			rcols.add(pair.getRight());
			lcomps.add(new SortFunc(pair.getLeft(), true));
			rcomps.add(new SortFunc(pair.getRight(), true));
		}
		Comparator<Tuple> lcomp = new LexicographicSort(lcomps);
		Comparator<Tuple> rcomp = new LexicographicSort(rcomps);
		this.opL = new Sort(opL, lcomp);
		this.opR = new Sort(opR, rcomp);
		this.joinColumns = joinColumns;
	}

	@Override
	public boolean moveNext() {
		switch(s) {
		case INIT:
			if(opL.moveNext() && opR.moveNext()) {
				s = SortMergeStatus.NO_BUFFER;
				// DROP THROUGH TO NO_BUFFER
			} else {
				s = SortMergeStatus.FINISHED;
				return false;
			}
		case NO_BUFFER:
			current = null;
			int cmp = compareOperators();
			while(cmp != 0) {
				if(cmp < 0) {
					if(!opL.moveNext()) {
						s = SortMergeStatus.FINISHED;
						return false;
					}
				} else {
					if(!opR.moveNext()) {
						s = SortMergeStatus.FINISHED;
						return false;
					}
				}
				cmp = compareOperators();
			}
			// Now we now that left and right are equal (in term of join pairs)
			// Both buffers have at least one element.
			s = SortMergeStatus.FROM_BUFFER;
			// Set status first, might be overriden!
			leftBuffer = new Stack<Tuple>();
			leftBuffer.addAll(joinBuffer(opL, lcols));
			rightBuffer = joinBuffer(opR, rcols);
		case FROM_BUFFER:
		case LAST_BUFFER:
			Tuple left = leftBuffer.peek();
			Tuple right = rightBuffer.get(rightIndex);
			rightIndex += 1;
			current = SortMerge.join(left, right, joinColumns);
			if(rightIndex >= rightBuffer.size()) {
				rightIndex = 0;
				leftBuffer.pop();
				if(leftBuffer.isEmpty() && s != SortMergeStatus.FINISHED) {
					if(s == SortMergeStatus.LAST_BUFFER) {
						s = SortMergeStatus.FINISHED;
					} else {
						s = SortMergeStatus.NO_BUFFER;
						rightBuffer = null;
					}
				}
			}
			return true;
		case FINISHED:
			current = null;
			return false;
		}
		return false;
	}

	private int compareOperators() {
		if(opL.current == null) {
			return -1;
		} else if(opR.current == null) {
			return 1;
		}
		int result = 0;
		for(JoinPair<String> pair: joinColumns) {
			String left = opL.current.get(pair.getLeft());
			String right = opR.current.get(pair.getRight());
			result = left.compareTo(right);
			if(result != 0) {
				break;
			}
		}
		return result;
	}

	// Cursor is after the last equal tuple
	private List<Tuple> joinBuffer(Operator op, List<String> cols) {
		List<Tuple> buffer = new ArrayList<Tuple>();
		String[] values = op.current.getValues(cols);
		buffer.add(op.current);
		if(!op.moveNext()) {
			s = SortMergeStatus.LAST_BUFFER;
			return buffer;
		}
		while(Arrays.equals(values, op.current.getValues(cols))) {
			buffer.add(op.current);
			if(!op.moveNext()) {
				s = SortMergeStatus.LAST_BUFFER;
				return buffer;
			}
		} 
		return buffer;
	}

	public static final Tuple join(Tuple left, Tuple right, List<JoinPair<String>> columns) {
		return Join.leftUnionOnJoinColumns(left, right, columns);
	}

	private static enum SortMergeStatus {
		INIT, FROM_BUFFER, NO_BUFFER, LAST_BUFFER, FINISHED;
	}

	@Override
	public String toString() {
		return "JOIN (" + opL + ") WITH (" + opR + ") ON " + joinColumns + " BY SORT MERGE";
	}
}
