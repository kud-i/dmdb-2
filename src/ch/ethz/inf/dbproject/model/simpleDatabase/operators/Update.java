package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.io.IOException;
import java.util.List;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleIdentifier;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleSchema;
import ch.ethz.inf.dbproject.model.simpleDatabase.storage.*;

public class Update extends Operator {
	private static final TupleSchema RS = new TupleSchema(new String [] {
			"pageNr", "slotNr"
	});
	private final PageManager into;
	private final Operator source;
	private final List<Updator> updators;

	public Update(final Operator source, List<Updator> updators, final PageManager into) {
		this.into = into;
		this.source = source;
		this.updators = updators;
	}

	@Override
	public boolean moveNext() {
		if(source.moveNext()) {
			try {
				Tuple t = source.current();
				Index idx = into.getPrimaryKey();
				TupleIdentifier tid = idx.get(t.getValues(idx.getKeys()));
				Page p = into.load(tid.getPageNr(), true);
				Tuple ot = p.getSlot(tid.getSlotNr());
				Tuple nt = ot;
				for(Updator u: updators) {
					nt = u.applyTo(nt);
				}
				p.update(tid.getSlotNr(), nt);
				current = new Tuple(RS, new String[] {
						Integer.toString(tid.getPageNr()), Integer.toString(tid.getSlotNr()) });
			} catch (IOException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}
	
	@Override
	public void close() {
		try {
			into.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
