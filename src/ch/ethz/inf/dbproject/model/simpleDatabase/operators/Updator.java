package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.util.Arrays;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

public class Updator {
	private final String key, value;
	
	public Updator(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	public Tuple applyTo(Tuple t) {
		String[] values = Arrays.copyOf(t.getValues(), t.getValues().length);
		int i = t.getSchema().getIndex(key);
		values[i] = value;
		return new Tuple(t.getSchema(), values);	
	}
}
