package ch.ethz.inf.dbproject.model.simpleDatabase.sqlParser;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import ch.ethz.inf.dbproject.model.simpleDatabase.JoinPair;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleSchema;
import ch.ethz.inf.dbproject.model.simpleDatabase.operators.*;
import ch.ethz.inf.dbproject.model.simpleDatabase.storage.PageManager;
import ch.ethz.inf.dbproject.model.simpleDatabase.storage.TableManager;

public final class Parser {
	private final TableManager tmy;
	public Parser(TableManager tmy) {
		this.tmy = tmy;
	}

	public final Operator parseSQL(String sqlQuery) {
		//Cut out DB connection informations
		sqlQuery = sqlQuery.substring(sqlQuery.indexOf(":")+1).trim() ;
		
		//Mask ` & ; & '
		sqlQuery = sqlQuery.replaceAll("`", "");
		sqlQuery = sqlQuery.replaceAll(";", "");
		sqlQuery = sqlQuery.replaceAll("'", "");
		
		//FIXME
		System.out.println("Working Directory = " + System.getProperty("user.dir"));
		System.out.println(sqlQuery);
		
		if (sqlQuery.contains("INSERT INTO")){
			return InsertIntoStatement(sqlQuery.trim());
			
		}else if (sqlQuery.contains("UPDATE")){	
			return UpdateStatement(sqlQuery.trim());
			
		}else if (sqlQuery.contains("DELETE")){
			return DeleteStatement(sqlQuery.trim());
			
		}else if (sqlQuery.contains("REPLACE")){	
			return ReplaceStatement(sqlQuery.trim());
			
		}else if (sqlQuery.contains("NOT IN")){
			return NotInStatment(sqlQuery.trim());
					
		}else if (sqlQuery.contains("SELECT")){			
			return SelectStatement(sqlQuery.trim());
			
		}else{
			//ERROR	
			System.out.println("Error");
			return null;
		}			
	}	
	
	
	private Operator NotInStatment(String sqlQuery) {
		//NOT IN has to be the last condition.
		Operator op = null;
		Operator subOp = null;
		String argOp = "";
		String argSubOp = "";
		//SELECT * FROM Poi WHERE poiId NOT IN (SELECT poiId FROM Involved where caseNr = 2)
		String[] query = sqlQuery.split("NOT IN");
		
		subOp = SelectStatement(query[1].substring(2, query[1].length()-1));
		argSubOp = query[1].substring(query[1].indexOf("SELECT")+6,query[1].indexOf("FROM")).trim();
		
		//Check if other conditions than NOT IN 
		int c = -1;
		if (query[0].lastIndexOf(",") < query[0].indexOf("WHERE"))
			c = query[0].indexOf("WHERE")+5;
		else 
			c = query[0].lastIndexOf(",") +1;
	
		op = SelectStatement(query[0].substring(0,c).trim());
		argOp = query[0].substring(c,query[0].length()).trim();
		
		
		op = new NotIn(op,subOp,argOp,argSubOp);
		
		
		return op;
	}

	private Operator UpdateStatement(String sqlQuery) {
		//Replace Null
		sqlQuery = sqlQuery.replace("NULL", "");
		
		Operator op = null;
		
		int u = sqlQuery.indexOf("UPDATE");
		int s = sqlQuery.indexOf("SET");
		int w = sqlQuery.indexOf("WHERE");
		
		String tableName = sqlQuery.substring(u+6,s).trim();
		String updator = sqlQuery.substring(s+3,w);
		String conditions = sqlQuery.substring(w+5, sqlQuery.length()).trim();
		
		//Updators
		List<Updator> updators = new LinkedList<Updator>();
		
		String[] setTuples = updator.split(",");
		for(String tuple: setTuples) { 
			String[] t = tuple.split("\\=");
			updators.add(new Updator(t[0].trim(),t[1].trim()));			
		}
		
		//Scan
		try {
 			PageManager table = tmy.getTable(tableName);
 			op = new PagedScan(table);
		
		//Select
		op = processSingleTableSelect(op, conditions);
		
		//Update
		op = new Update(op, updators, table);
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return op;
	}

	private Operator ReplaceStatement(String sqlQuery) {
		Operator op = null;
		
		String[] query = sqlQuery.split("VALUE");
		
		int i = query[0].indexOf("REPLACE");
		int k = query[0].indexOf("(");
		
		query[0] = query[0].trim();
		query[1] = query[1].trim();
		
		String table = query[0].substring(i+7,k).trim();
		String columns = query[0].substring(k+1,query[0].length()-1).trim();
		String values = query[1].substring(1,query[1].length()).trim();
		
		//Columns
		StringTokenizer st2 = new StringTokenizer(columns, ",");
	
		String[] col = new String[st2.countTokens()];
		int j = 0;
		while (st2.hasMoreElements()) {
			col[j] = st2.nextToken().trim();
			j+=1;
		}
		
		//Values
		op = new Scan(values, col);
				
		//Replace
		try {
			PageManager	pm = tmy.getTable(table);
			op = new Replace(op, pm);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return op;
	}

	private Operator DeleteStatement(String sqlQuery) {
		
		Operator op = null;
		
		int d = sqlQuery.indexOf("DELETE FROM");
		int w = sqlQuery.indexOf("WHERE");
		
		String table = sqlQuery.substring(d+11,w).trim();
		String conditions = sqlQuery.substring(w+5, sqlQuery.length()).trim();
		
		//Scan
		op = processSingleScan(table);
		
		//Select
		op = processSingleTableSelect(op,conditions);
		
		//Delete
		try {
			PageManager	pm = tmy.getTable(table);
			op = new Delete(op, pm);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return op;
	}

	private Operator InsertIntoStatement(String sqlQuery) {
		//Replace Null
		sqlQuery = sqlQuery.replaceAll("NULL", "");
		
		Operator op = null;
		
		if (sqlQuery.contains("SELECT"))
			op = processSelectInsert(sqlQuery);
		else
			op = processSimpleInsert(sqlQuery);

				
		return op;
	}

	private Operator processSimpleInsert(String sqlQuery) {
		Operator op = null;
		
		String[] query = sqlQuery.split("VALUES");
		
		int i = query[0].indexOf("INSERT INTO");
		int k = query[0].indexOf("(");
		
		query[0] = query[0].trim();
		query[1] = query[1].trim();
		
		String table = query[0].substring(i+11,k).trim();
		String columns = query[0].substring(k+1,query[0].length()-1).trim();
		String values = query[1].substring(0,query[1].length());
	
		//Columns
		StringTokenizer st2 = new StringTokenizer(columns, ",");
	
		String[] col = new String[st2.countTokens()];
		int j = 0;
		while (st2.hasMoreElements()) {
			col[j] = st2.nextToken().trim();
			j+=1;
		}
		
		//Values
		op = new Scan(values.trim(), col);
			
		//Insert
		try {
			PageManager	pm = tmy.getTable(table);
			op = new Insert(op, pm, new TupleSchema(col));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return op;
	}

	private Operator processSelectInsert(String sqlQuery) {
		Operator op = null;
		
		int i = sqlQuery.indexOf("INSERT INTO");
		int k = sqlQuery.indexOf("(");
		int s = sqlQuery.indexOf("SELECT");
		
		String table = sqlQuery.substring(i+11,k).trim();
		String columns = sqlQuery.substring(k+1,s-1).trim();
		String select = sqlQuery.substring(s, sqlQuery.length());
		
		//Columns
		StringTokenizer st2 = new StringTokenizer(columns, ",");
	
		String[] col = new String[st2.countTokens()];
		int j = 0;
		while (st2.hasMoreElements()) {
			col[j] = st2.nextToken().trim();
			j+=1;
		}
		
		//Select
		op = SelectStatement(select);
				
		//Insert
		try {
			PageManager	pm = tmy.getTable(table);
			op = new Insert(op, pm, new TupleSchema(col));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return op;
	}

	private Operator SelectStatement(String sqlQuery){
		Operator op = null;
		
		int s = sqlQuery.indexOf("SELECT");
		int w = sqlQuery.indexOf("WHERE");
		int f = sqlQuery.indexOf("FROM");
		int o = sqlQuery.indexOf("ORDER BY");
		int l = sqlQuery.indexOf("LIMIT");
		
		String columns = "";
		String table = "";
		String conditions = "";
		String order = "";
		String limit = "";
		
		if (s>= 0 && f>=0){
			//get raw column data
			columns = sqlQuery.substring(s+6, f).trim();	
		}
		if (f>=0 && w>=0){
			//get raw tables
			table = sqlQuery.substring(f+4, w).trim();
		}else if(f>=0 && o>=0){
			table = sqlQuery.substring(f+4, o).trim();
		}else if(f>=0 && l>=0){
			table = sqlQuery.substring(f+4, l).trim();
		}else if(f>=0){
			table = sqlQuery.substring(f+4, sqlQuery.length()).trim();
		}
		if (w>=0 && o>=0){
			//get raw conditions (select & join)
			conditions = sqlQuery.substring(w+5, o).trim();
		}else if (w>=0 && l>0){
			conditions = sqlQuery.substring(w+5, l).trim();
		}else if (w>=0){
			conditions = sqlQuery.substring(w+5, sqlQuery.length()).trim();
		}
		if (o>=0 && l>=0){
			//get raw order column
			order = sqlQuery.substring(o+8,l).trim();
		}else if (o>=0){
			order = sqlQuery.substring(o+8,sqlQuery.length()).trim();
		}
		if (l>=0)
			//get raw limit
			limit = sqlQuery.substring(l+5,sqlQuery.length()).trim();
		
			
		
		//Join SQLs - tables and columns need Identifiers!
		//Order of Tables after FROM gives join order.
		if (table.contains(",")){
			//Joinpairs in Join order
			ArrayList<JoinPair<String>> joinPairs = getJoinPairs(conditions,table);
					
			String[] tables = table.split("\\,");
			
			Operator[] mop = new Operator[tables.length];
			int i = 0;
			for (String t:tables){
				t = t.trim();
				String[] currTable = t.split("\\ ");
				
				//Scan
				mop[i]=processSingleScan(currTable[0]);
				
				//Select
				mop[i] = processMultiTableSelect(mop[i], conditions, currTable[1]);
				
				i+=1;
			}
			
			
			//Join
			for(i = 1; i < mop.length; i++){
				if (i == 1)
					op= new SortMerge(mop[0], mop[1], joinPairs);
				else
					op = new SortMerge(op, mop[i], joinPairs);
			}
			
		}else{
			
			//Scan
			op = processSingleScan(table.trim());
			
			//Select
			op = processSingleTableSelect(op,conditions);
		}
		
		
		//Sort
		op = processSort(op,order);
		
		//Project
		op = processProject(op,columns);
		
		//Limit
		op = processLimit(op,limit);
		
		return op;
	}
	
	
	private Operator processLimit(Operator op, String limit) {
		if (limit.length() > 0){
			int l = Integer.parseInt(limit);
			op = new Limit(op,l);
		}
		return op;
	}

	private Operator processSingleScan(String tableName){
		Operator op = null;
		try {
 			PageManager table = tmy.getTable(tableName);
 			op = new PagedScan(table);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return op;
		
	}
	
	private Operator processSingleTableSelect(Operator op, String conditions){

		if (conditions.length() >0){

			if (!conditions.contains("OR")){
				String[] andTuples = conditions.split("AND");
				for(String tuple: andTuples) { 

					if (tuple.contains("=")){
						String[] s = tuple.split("\\=");
						if (s.length == 2){
							op = new Select(op,s[0].trim(),s[1].trim(),SelectorFuncs.EQUAL);
						}else{
							op = new Select(op,s[0].trim(),"",SelectorFuncs.EQUAL);
						}
					}else if (tuple.contains("<>")){

						String[] s = tuple.split("<>");
						if (s.length == 2)
							op = new Select(op,s[0].trim(),s[1].trim(),SelectorFuncs.NOT_EQUAL);
						else
							op = new Select(op,s[0].trim(),"",SelectorFuncs.NOT_EQUAL);

					}else if (tuple.contains("IS NULL")){
						String[] s = tuple.split("IS");
						op = new Select(op,s[0].trim(),"",SelectorFuncs.EQUAL);

					}else if (tuple.contains("LIKE")){
						String[] s = tuple.split("LIKE");
						if (s.length == 2){
							op = new Select(op,s[0].trim(),s[1].trim(),SelectorFuncs.LIKE);
						}else{
							op = new Select(op,s[0].trim(),"",SelectorFuncs.LIKE);
						}
					}
				}
			}else{
				String[] orTuples = conditions.split("OR");
				String[] columns = new String[orTuples.length];
				String[] values = new String[orTuples.length];
				int i= 0;
				for(String tuple: orTuples) {
					if (tuple.contains("LIKE")){
						String[] s = tuple.split("LIKE");
						if (s.length == 2){
							columns[i] = s[0].trim();
							values[i] = s[1].trim();
						}else{
							columns[i] = s[0].trim();
							values[i] = "";
						}
					}
					i+=1;
				}
				op = new SelectLikeOr(op,columns,values);
			}
			
			
		}		
		return op;
	}
	
	private Operator processMultiTableSelect(Operator op, String conditions,String tablePrefix){
		if (conditions.length() >0){
			String[] andTuples = conditions.split("AND");
			for(String tuple: andTuples) { 

				if (tuple.contains("=")){
					String[] s = tuple.split("\\=");
					if (s[0].contains(tablePrefix +".") && !s[1].contains(".")){
						String[] s0 = s[0].split("\\.");
						if (s.length == 2){
							op = new Select(op,s0[1].trim(),s[1].trim(),SelectorFuncs.EQUAL);
						}else{
							op = new Select(op,s0[1].trim(),"",SelectorFuncs.EQUAL);
						}
					}

				}else if (tuple.contains("<>")){
					String[] s = tuple.split("<>");
					if (s[0].contains(tablePrefix +".") && !s[1].contains(".")){
						String[] s0 = s[0].split("\\.");
						if (s.length == 2)
							op = new Select(op,s0[1].trim(),s[1].trim(),SelectorFuncs.NOT_EQUAL);
						else
							op = new Select(op,s0[1].trim(),"",SelectorFuncs.NOT_EQUAL);
					}

				}else if (tuple.contains("IS NULL")){
					String[] s = tuple.split("IS");
					if (s[0].contains(tablePrefix +".") && !s[1].contains(".")){
						String[] s0 = s[0].split("\\.");
						op = new Select(op,s0[1],"",SelectorFuncs.EQUAL);
					}
					
				}else if (tuple.contains("LIKE")){
					String[] s = tuple.split("LIKE");
					if (s[0].contains(tablePrefix +".") && !s[1].contains(".")){
						String[] s0 = s[0].split("\\.");
						if (s.length == 2)
						op = new Select(op,s0[1],s[1],SelectorFuncs.LIKE);
						else
						op = new Select(op,s0[1],"",SelectorFuncs.LIKE);

					}
				}
			}
		}

		return op;
	}
	
	private Operator processProject(Operator op, String columns){
		if (!columns.contains("*"))
			op = new Project(op,columns.split(","));
		
		return op;
	}
	
	private Operator processSort(Operator op, String order){
		if (order.length() > 0){
			if (order.contains("DESC")){
				order = order.substring(0,order.indexOf("DESC"));
				op = new Sort(op,order.trim(),false);
				
			}else{
				op = new Sort(op,order.trim(),true);
			}
		}
		return op;
	}
	
	private ArrayList<JoinPair<String>> getJoinPairs (String conditions, String tables){
		ArrayList<JoinPair<String>> joinPair= new ArrayList<JoinPair<String>>();
		
		if (conditions.length() >0){
			String[] andTuples = conditions.split("AND");
			for(String tuple: andTuples) { 
				String[] s = tuple.split("\\=");
				if (s[0].contains(".") && s[1].contains(".")){
					String[] s0 = s[0].split("\\.");
					String[] s1 = s[1].split("\\.");
					
					if (tables.indexOf(" " + s0[0]) <= tables.indexOf(" " + s1[0]))
						joinPair.add(new JoinPair<String>(s0[1].trim(),s1[1].trim()));
					else
						joinPair.add(new JoinPair<String>(s1[1].trim(),s0[1].trim()));				
				}
			}
		}
		
		
		
		return joinPair;
	}
	
	
}