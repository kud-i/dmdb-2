package ch.ethz.inf.dbproject.model.simpleDatabase.storage;

public class AlreadyExistException extends Exception {
	private static final long serialVersionUID = -6683705338970485080L;

	public AlreadyExistException(String key) {
		super(key + " already in Index");
	}
}
