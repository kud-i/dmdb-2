package ch.ethz.inf.dbproject.model.simpleDatabase.storage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleIdentifier;

public class AutoIncrementer implements Index {
	private final String[] keys;
	private final int[] next;

	public AutoIncrementer(String[] keys) {
		this.keys = keys;
		next = new int[keys.length];
		for(int i = 0; i < keys.length; i++) {
			next[i] = 0;
		}
	}

	@Override
	public void add(Tuple t) throws AlreadyExistException {
		String[] values = t.getValues(keys);
		for(int i = 0; i < values.length; i++) {
			int value = Integer.parseInt(values[i]);
			if(value >= next[i]) {
				next[i] = value + 1;
			}
		}
	}

	@Override
	public TupleIdentifier get(String[] values) {
		return null;
	}

	@Override
	public TupleIdentifier remove(Tuple t) {
		return null;
	}

	public int[] getNext() {
		return next;
	}

	@Override
	public String[] getKeys() {
		return keys;
	}

	@Override
	public void serializeTo(File indexFile) throws IOException {
		DataOutputStream ds = new DataOutputStream(new FileOutputStream(indexFile));
		for(int n: next) {
			ds.writeInt(n);
		}
		ds.close();
	}

	@Override
	public void readIn(InputStream is) throws IOException {
		DataInputStream di = new DataInputStream(is);
		for(int i = 0; i < next.length; i++) {
			next[i] = di.readInt();
		}
		di.close();
		is.close();
	}
}
