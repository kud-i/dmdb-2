package ch.ethz.inf.dbproject.model.simpleDatabase.storage;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleIdentifier;

public interface Index {
	String[] getKeys();
	void add(Tuple t) throws AlreadyExistException;
	TupleIdentifier get(String[] values);
	TupleIdentifier remove(Tuple t);
	
	void readIn(InputStream is) throws IOException;
	void serializeTo(File indexFile) throws IOException;
}
