package ch.ethz.inf.dbproject.model.simpleDatabase.storage;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleIdentifier;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleSchema;

/**
 * Layout is
 * content slotPointers slotUsed slotPage free slotCount
 * @author timethy
 *
 */
public class Page {
	public final int pageNr;
	private final PageManager manager;
	private final TupleSchema schema;
	private final Collection<Index> indices;

	public Page(PageManager manager, int pageNr) {
		this.manager = manager;
		this.schema = manager.schema;
		this.pageNr = pageNr;
		this.indices = manager.getIndices();
	}

	int free; // How much free space we have left, can be fragmented in slots
	private byte[] content; // Content from beginning, w/o slot info
	private ArrayList<Integer> slotPointers; // Pointer into content array
	private ArrayList<Integer> slotUsed; // Space used | slotNr if forward
	private ArrayList<Integer> slotPage; // page is negative if slot is deleted, and the pageNr otherwise
	
	public int lastAvailable() {
		return lastSlot() < 0 ? free :
			PageManager.PAGE_SIZE - 8 - slotPointers.get(lastSlot()) - slotUsed.get(lastSlot());
	}

	public int lastSlot() {
		return slotPointers.size() - 1;
	}
	
	public boolean notForwarded(int slotNr) {
		return pageNr == slotPage.get(slotNr);
	}

	public Tuple getSlot(int slotNr) {
		int pageNr = slotPage.get(slotNr);
		if(pageNr == this.pageNr) {
			return schema.parse(content, slotPointers.get(slotNr), slotUsed.get(slotNr), pageNr, slotNr);
		} else if(pageNr >= 0) {
			try {
				return manager.load(pageNr, false).getSlot(slotUsed.get(slotNr));
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		} else {
			return null;
		}
	}

	public void update(int slotNr, Tuple t) {
		// check if slot is forwarded first.
		if(slotPage.get(slotNr) != this.pageNr) {
			try {
				manager.load(slotPage.get(slotNr), false).update(slotNr, t);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		byte[] b = t.getBytes();
		int oldLength = slotUsed.get(slotNr);
		int o = slotPointers.get(slotNr);
		int no = slotNr + 1 < slotPointers.size() ? slotPointers.get(slotNr + 1) : content.length;
		int avail = no - o;
		if(avail >= b.length) {
			slotUsed.set(slotNr, b.length);
			free = free + oldLength - b.length;
			for(Index i: indices) {
				i.remove(getSlot(slotNr));
			}
			// we're fine, let's copy over b
			for(int i = 0; i < b.length; i++) {
				content[o+i] = b[i];
			}
			for(Index i: indices) {
				try {
					i.add(t.withIdentifier(new TupleIdentifier(pageNr, slotNr)));
				} catch (AlreadyExistException e) {
					assert false: "Index still exist, we should have removed it.";
				}
			}
		} else {
			// we rearrange page or we're screwed and escalate to another page (via the PageManager)
			if(free >= b.length) {
				free = free + oldLength - b.length;
				rearrangePage(slotNr, b.length);
				o = slotPointers.get(slotNr);
				for(int i = 0; i < b.length; i++) {
					content[o+i] = b[i];
				}
				slotUsed.set(slotNr, b.length);
			} else {
				forward(slotNr, t);
			}
		}
	}
	
	public void delete(int slotNr) {
		// check if slot is forwarded first.
		if(slotPage.get(slotNr) != this.pageNr) {
			try {
				manager.load(slotPage.get(slotNr), false).delete(slotNr);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		for(Index i: indices) {
			i.remove(getSlot(slotNr));
		}
		slotPage.set(slotNr, -1);
		slotUsed.set(slotNr, 0);
	}
	
	private Tuple prepareTuple(Tuple t) {
		String[] values = new String[manager.schema.getColumns().length];
		// First set all columns given through the to be inserted Tuple
		for(String key: t.getSchema().getColumns()) {
			int j = manager.schema.getIndex(key);
			values[j] = t.get(key);
		}
		// Now fill in the missing columns with auto increments
		String[] incs = manager.getIncrementer().getKeys();
		int[] next = manager.getIncrementer().getNext();
		for(int i = 0; i < incs.length; i++) {
			int j = manager.schema.getIndex(incs[i]);
			if(values[j] == null) {
				values[j] = Integer.toString(next[i]);
			}
		}
		for(int i = 0; i < values.length; i++) {
			if(values[i] == null) {
				values[i] = "";
			}
		}
		// Return prepared Tuple
		return new Tuple(manager.schema, values);
	}
	
	public Tuple prepareAndAppend(Tuple t) throws AlreadyExistException {
		return append(prepareTuple(t));
	}

	private Tuple append(Tuple t) throws AlreadyExistException {
		byte[] b = t.getBytes();
		// TODO: Nice to have, try to reuse a slot (saves 12 bytes instead of creating new one)
		if(free - 12 >= b.length) {
			int slotNr = slotPointers.size();
			t = t.withIdentifier(new TupleIdentifier(pageNr, slotNr));
			for(Index i: indices) {
				i.add(t); // Possibly throws AlreadyExistException, don't continue after!
			}
			free -= 12; // Allocate bytes from end for pointers, used and flags.
			if(lastAvailable() < b.length) {
				rearrangePage(slotNr, b.length);
			}
			int o = (slotNr == 0 ? 0 : slotPointers.get(slotNr - 1) + slotUsed.get(slotNr - 1));
			// We can safely add here, since we are adding the last element.
			slotPointers.add(slotNr, o);
			slotPage.add(slotNr, this.pageNr);
			slotUsed.add(slotNr, b.length);
			for(int i = 0; i < b.length; i++) {
				content[o+i] = b[i];
			}
			free -= b.length;
			return t;
		} else {
			assert false;
			// Can't insert here.
			return null;
		}
	}
	
	private void forward(int slotNr, Tuple t) {
		try {
			Page newPage = manager.getInsertPage(t);
			for(Index i: indices) {
				i.remove(t);
			}
			int newSlotNr = newPage.append(t).getSlotNr();
			if(newSlotNr < 0) {
				throw new IOException("Not enough space in given page!");
			}
			slotPage.set(slotNr, newPage.pageNr);
			slotUsed.set(slotNr, newSlotNr);
			int next = slotNr + 1 < slotPointers.size() ? slotPointers.get(slotNr + 1) : content.length;
			slotPointers.set(slotNr, next);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (AlreadyExistException e) {
			assert false: "Index still exist, we should have removed it.";
		}
	}
	
	/**
	 * Compact a page and make sure, slotNr has `bcount` bytes available.
	 */
	private void rearrangePage(int slotNr, int bcount) {
		byte[] newContent = new byte[content.length];
		int b = 0;
		for(int i = 0; i < slotPointers.size(); i++) {
			int offset = slotPointers.get(i);
			slotPointers.set(i, b);
			if(slotPage.get(i) == this.pageNr) {
				int len = i == slotNr ? bcount : slotUsed.get(i);
				for(int x = 0; x < len; x++) {
					newContent[b] = content[offset + x];
					b += 1;
				}
			} else {
				slotUsed.set(i, 0);
			}
		}
		content = newContent;
	}
	
	public void initialize() {
		int slotCount = 0;
		free = PageManager.PAGE_SIZE - 8;
		slotPointers = new ArrayList<Integer>(slotCount);
		slotUsed = new ArrayList<Integer>(slotCount);
		slotPage = new ArrayList<Integer>(slotCount);
		content = new byte[free];
	}
	
	public void readIn(ByteBuffer buff) {
		// Read from end
		int slotCount = buff.getInt(PageManager.PAGE_SIZE - 4);
		free = buff.getInt(PageManager.PAGE_SIZE - 8);
		slotPointers = new ArrayList<Integer>(slotCount);
		slotUsed = new ArrayList<Integer>(slotCount);
		slotPage = new ArrayList<Integer>(slotCount);
		int offset = PageManager.PAGE_SIZE - 8;
		
		// We want the indices to be avail.
		if(slotCount != 0) {
			for(int i = 0; i < slotCount; i++) {
				slotPointers.add(null);
				slotUsed.add(null);
				slotPage.add(null);
			}
		}

		for(int i = slotCount - 1; i >= 0; i--) {
			offset -= 4;
			slotPage.set(i, buff.getInt(offset));
		}
		for(int i = slotCount - 1; i >= 0; i--) {
			offset -= 4;
			slotUsed.set(i, buff.getInt(offset));
		}
		for(int i = slotCount - 1; i >= 0; i--) {
			offset -= 4;
			slotPointers.set(i, buff.getInt(offset));
		}
		content = new byte[offset];
		buff.position(0);
		buff.get(content);
	}

	public void writeTo(ByteBuffer buff) {
		int slotCount = slotPointers.size();
		int contentLength = PageManager.PAGE_SIZE - 8 - 12*slotCount;
		buff.position(0);
		buff.put(content, 0, contentLength);
		for(int i: slotPointers) {
			buff.putInt(i);
		}
		for(int i: slotUsed) {
			buff.putInt(i);
		}
		for(int i: slotPage) {
			buff.putInt(i);
		}
		buff.putInt(free);
		buff.putInt(slotCount);
	}
}
