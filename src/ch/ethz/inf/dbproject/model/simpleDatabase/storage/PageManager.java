package ch.ethz.inf.dbproject.model.simpleDatabase.storage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ch.ethz.inf.dbproject.model.simpleDatabase.*;

public class PageManager {
	public static final int PAGE_SIZE = 8192;
	
	private final String tableName;

	// One page manager per table
	public final TupleSchema schema;
	private final File indexDir;
	private RandomAccessFile pageFile;

	private Index primaryKey;
	private AutoIncrementer incrementer;
	private final Map<String, Index> indices = new HashMap<String, Index>();
	private final Map<Integer, Page> pageCache = new HashMap<>();
	
	// TODO: Min-heap for min-fit approach for pages.
	// Use append-only for now
	private int pageCount;
	
	public PageManager(String tableName, String fileName, TupleSchema schema, File indexDir) {
		this.tableName = tableName;
		this.indexDir = indexDir;
		this.schema = schema;
		try {
			createIndices();
			this.pageFile = new RandomAccessFile(fileName, "rw");
			this.pageCount = (int) this.pageFile.length() / PAGE_SIZE;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void purge() throws IOException {
		pageFile.setLength(0);
		pageCount = 0;
		pageCache.clear();
		if(indexDir.exists()) { 
			for(File child: indexDir.listFiles()) {
				child.delete();
			}
		}
		createIndices();
	}

	public void flush() throws IOException {
		for(Page p: pageCache.values()) {
			save(p);
		}
		pageCache.clear();
		flushIndices();
	}

	public void flush(Page page) throws IOException {
		pageCache.remove(page.pageNr);
		save(page);
		flushIndices();
	}

	private void flushIndices() throws IOException {
		for(Map.Entry<String, Index> e: indices.entrySet()) {
			e.getValue().serializeTo(new File(indexDir, e.getKey()));
		}
	}

	private void createIndices() throws IOException {
		primaryKey = new SimpleMemoryIndex(schema.getPrimaryKey());
		indices.put("primary", primaryKey);
		incrementer = new AutoIncrementer(schema.getAutoIncrement());
		if(schema.getAutoIncrement().length > 0) {
			indices.put("auto-inc", incrementer);
		}
		for(Map.Entry<String, Index> e: indices.entrySet()) {
			File indexFile = new File(indexDir, e.getKey());
			if(indexFile.exists()) {
				e.getValue().readIn(new FileInputStream(indexFile));
			}
		}
	}

	public AutoIncrementer getIncrementer() {
		return incrementer;
	}

	public Index getPrimaryKey() {
		return primaryKey;
	}

	public Collection<Index> getIndices() {
		return indices.values();
	}

	private Page createPage() throws IOException {
		Page p = new Page(this, pageCount);
		p.initialize();
		pageFile.setLength(pageCount * PAGE_SIZE);
		save(p);
		// put into cache after save, since save would remove the cached page
		pageCache.put(pageCount, p);
		pageCount += 1;
		return p;
	}

	/**
	 * @param cache Specify, whether we actually want to cache (or we are just running through)
	 */
	public Page load(int pageNr, boolean cache) throws IOException {
		if(pageNr >= pageCount) {
			return null;
		}
		Page p = pageCache.get(pageNr);
		if(p != null) {
			return p;
		}
		ByteBuffer dst = ByteBuffer.allocate(PAGE_SIZE);
		FileChannel channel = pageFile.getChannel();
		int read = channel.read(dst, pageNr*PAGE_SIZE);
		p = new Page(this, pageNr);
		p.readIn(dst);
		if(read != PAGE_SIZE) {
			throw new IOException("Invalid number of bytes read: " + read);
		}
		if(cache) {
			pageCache.put(pageNr, p);
		}
		return p;
	}

	private void save(Page page) throws IOException {
		ByteBuffer buff = ByteBuffer.allocate(PAGE_SIZE);
		page.writeTo(buff);
		buff.position(0);
		FileChannel channel = pageFile.getChannel();
		channel.write(buff, page.pageNr*PAGE_SIZE);
	}

	public Page getInsertPage(Tuple tuple) throws IOException {
		if(pageCount == 0) {
			return createPage();
		}
		Page candidate = load(pageCount - 1, true);
		if(candidate == null || candidate.free < tuple.getBytes().length) {
			return createPage();
		}
		return candidate;
	}

	public String getTableName() {
		return tableName;
	}
}
