package ch.ethz.inf.dbproject.model.simpleDatabase.storage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleIdentifier;

public class SimpleMemoryIndex implements Index {
	private final String[] keys;
	private final TreeMap<String,TupleIdentifier> tuples
		= new TreeMap<String,TupleIdentifier>();
	private final Set<String> dirty = new TreeSet<String>();
	private boolean rewrite = false;

	public SimpleMemoryIndex(String[] keys) {
		this.keys = keys;
	}

	@Override
	public void add(Tuple t) throws AlreadyExistException {
		if(tuples.isEmpty()) {
			rewrite = true;
		}
		String key = t.getValueString(keys);
		if(tuples.containsKey(key)) {
			throw new AlreadyExistException(key);
		} else {
			tuples.put(key, t.getIdentifier());
		}
		dirty.add(key);
	}

	@Override
	public TupleIdentifier get(String[] values) {
		StringBuffer buff = new StringBuffer();
		for(String v: values) {
			buff.append(v);
			buff.append('\t');
		}
		return tuples.get(buff.toString());
	}

	@Override
	public TupleIdentifier remove(Tuple t) {
		String key = t.getValueString(keys);
		rewrite = true;
		dirty.clear();
		return tuples.remove(key);
	}

	@Override
	public String[] getKeys() {
		return keys;
	}

	@Override
	public void serializeTo(File indexFile) throws IOException {
		if(rewrite || tuples.isEmpty()) {
			dirty.clear();
			dirty.addAll(tuples.keySet());
			indexFile.delete();
		}
		RandomAccessFile f = new RandomAccessFile(indexFile, "rw");
		f.seek(0);
		f.writeInt(tuples.entrySet().size());
		f.seek(f.length());
		for(String key: dirty) {
			TupleIdentifier tid = tuples.get(key);
			f.writeUTF(key);
			f.writeInt(tid.getPageNr());
			f.writeInt(tid.getSlotNr());
		}
		f.close();
		dirty.clear();
		rewrite = false;
	}

	@Override
	public void readIn(InputStream is) throws IOException {
		DataInputStream di = new DataInputStream(is);
		int count = di.readInt();
		for(int i = 0; i < count; i++) {
			String k = di.readUTF();
			int pageNr = di.readInt();
			int slotNr = di.readInt();
			tuples.put(k, new TupleIdentifier(pageNr, slotNr));
		}
		di.close();
		is.close();
	}
}
