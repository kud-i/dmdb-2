package ch.ethz.inf.dbproject.model.simpleDatabase.storage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ch.ethz.inf.dbproject.model.simpleDatabase.TupleSchema;

public class TableManager {
	private final String directory;
	
	public TableManager(String directory) {
		this.directory = directory;
		new File(directory + "/data").mkdirs();
		new File(directory + "/schema").mkdirs();
	}

	private final Map<String, PageManager> tables = new HashMap<String, PageManager>();

	public PageManager createTable(String tableName, TupleSchema schema) throws IOException {
		saveSchema(tableName, schema);
		createDBFile(tableName);
		return getTable(tableName);
	}

	public PageManager getTable(String tableName) throws IOException {
		if(tables.containsKey(tableName)) {
			return tables.get(tableName);
		} else {
			createDBFile(tableName);
			PageManager pm = new PageManager(tableName,
					directory + "/data/" + tableName + ".db",
					readSchema(tableName),
					new File(directory + "/data/" + tableName + ".indices"));
			//tables.put(tableName, pm);
			return pm;
		}
	}
	public void flush() throws IOException {
		for(PageManager pm: tables.values()) {
			pm.flush();
		}
		tables.clear();
	}

	private void saveSchema(String tableName, TupleSchema schema) throws IOException {
		File schemaFile = new File(directory + "/schema/" + tableName + ".schema");
		BufferedWriter writer = new BufferedWriter(new FileWriter(schemaFile));
		for(String s: schema.getColumns()) {
			writer.write(s + "\n");
		}
		writer.write("#\n");
		for(String s: schema.getPrimaryKey()) {
			writer.write(s + "\n");
		}
		writer.write("#\n");
		for(String s: schema.getAutoIncrement()) {
			writer.write(s + "\n");
		}
		writer.close();
	}

	private void createDBFile(String tableName) throws IOException {
		File dbFile = new File(directory + "/data/" + tableName + ".db");
		dbFile.createNewFile();
		File indices = new File(directory + "/data/" + tableName + ".indices");
		indices.mkdir();
	}

	private TupleSchema readSchema(String tableName) throws IOException {
		File schemaFile = new File(directory + "/schema/" + tableName + ".schema");
		BufferedReader reader = new BufferedReader(new FileReader(schemaFile));
		List<String> columns = new LinkedList<String>();
		List<String> primaryKey = new LinkedList<String>();
		List<String> autoIncrement = new LinkedList<String>();
		String line = "";
		while(reader.ready()) {
			line = reader.readLine();
			if(line.equals("#")) {
				break;
			} else {
				columns.add(line);
			}
		}
		while(reader.ready()) {
			line = reader.readLine();
			if(line.equals("#")) {
				break;
			} else {
				primaryKey.add(line);
			}
		}
		while(reader.ready()) {
			line = reader.readLine();
			if(line.equals("#")) {
				break;
			} else {
				autoIncrement.add(line);
			}
		}
		reader.close();
		return new TupleSchema(
				columns.toArray(new String[columns.size()]),
				primaryKey.toArray(new String[primaryKey.size()]),
				autoIncrement.toArray(new String[autoIncrement.size()]));
	}
}
