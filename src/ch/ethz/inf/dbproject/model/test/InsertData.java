package ch.ethz.inf.dbproject.model.test;

import org.junit.Test;

import ch.ethz.inf.dbproject.model.simpleDatabase.operators.Operator;
import ch.ethz.inf.dbproject.model.simpleDatabase.sqlParser.Parser;
import ch.ethz.inf.dbproject.model.simpleDatabase.storage.TableManager;

public class InsertData {
	private TableManager tmy = new TableManager("db");
	private Parser parser = new Parser(tmy);

	@Test
	public void InsertUser(){
		String sql = ": INSERT INTO `User` (username,password) VALUES ('Bond','007'),('Conan','1234'),('demo','demo'),('kudi','1234'),('Sherlock','I<3Pizza');";
		Operator op = parser.parseSQL(sql);
		while (op.moveNext());
		op.close();
	}
	
	@Test
	public void InsertCase1(){
		String sql = ": INSERT INTO `Case` (caseNr,categoryName,title,open,date,location,description,username) VALUES " +
				"(1,'Drug related','Heisenberg','false','1267488000000','Albequerque','Undercover drug empire','Conan')," +
				"(2,'Drug related','Los Pollos','true','1267598000000','Albequerque','Undercover drug empire','Bond')," +
				"(3,'Hit and run','Accident on route 69','false,'1267598000000','Route 69','2 dead victims','Conan')," +
				"(4,'Speeding','Speeding 130 mph','true','1267238000000','Zurich','Corvette','Bond')," +
				"(5,'Auto theft','Corvette stolen','true,'1232598000000','Kauflauten','Red Corvette','Bond')";	
		Operator op = parser.parseSQL(sql);
		while (op.moveNext());
		op.close();
	}

	@Test
	public void InsertCase2(){
		String sql = ": INSERT INTO `Case` (caseNr,categoryName,title,open,date,location,description,username) VALUES" +
				" (6,'Murder','Assassination of  Kennedy','true','1267598000000','Dallas','Victim: John Fitzgerald Kennedy','Bond')" +
				",(7,'Insider trading','Facebook shares','true','126128000000','New York','Possible insider trade','kudi')," +
				"(8,'Burglary','Stolen Iphone','false','1261698000000','Basel','Phone in Bag','kudi')," +
				"(9,'Assault','Fight between two girls','true','1267598000000','Bellevue Zurich','Both over 60y','kudi')," +
				"(10,'Arson','House on fire','true','1245798000000','Seebach','Suburbs','Bond')," +
				"(11,'Drug trade','1','true','1269878000000','3','2','demo');";
		Operator op = parser.parseSQL(sql);
		while (op.moveNext());
		op.close();
	}
	
	@Test
	public void InsertCase3(){
		String sql = ": INSERT INTO `Case` (caseNr,categoryName,title,open,date,location,description,username) " +
				"VALUES (12,'Drug related','Felix','false','1267488000000','NY','Undercover drug empire','Conan')," +
				"(13,'Drug related','Timmy','true','1267598000000','Albe','Undercover drug empire','Bond')," +
				"(14,'Hit and run','Accident','false,'1262598000000','Route 32','3 dead victims','Conan')," +
				"(15,'Speeding','Speeding 130 mph','true','1267598000000','Geneva','Corvette','Bond')," +
				"(16,'Auto theft','Corvette stolen','true,'1267598000000','Landquart','Blue Corvette','Bond')," +
				"(17,'Auto theft','Ferrary stolen','true,'1243598000000','Basel','Red Corvette','Bond')," +
				"(18,'Hit and run','Accident','false,'1234598000000','Route 1','0 dead victims','Conan')," +
				"(19,'Drug related','Seebach','true','1222598000000','ABB','Undercover drug empire','Bond')," +
				"(20,'Speeding','Speeding 150 mph','true','1267598000000','Zurich','Fiat','Bond')," +
				"(21,'Auto theft','Corvette stolen','true,'1243598000000','Bern','Yellow Corvette','Bond')";
		Operator op = parser.parseSQL(sql);
		while (op.moveNext());
		op.close();
		
	}
	
	@Test
	public void InsertPNote(){
		String sql = ": INSERT INTO `PNote` (poiId,username,note) VALUES (1,'Bond',''),(1,'kudi','young blood'),(2,'kudi','timmy'),(3,'Bond','He is a very bad Taube'),(3,'Conan','Half Chinese half Taube'),(3,'Sherlock','Seems nice'),(4,'kudi','famous'),(7,'demo','1:demo\r\n2:demo2');";
		Operator op = parser.parseSQL(sql);
		while (op.moveNext());
		op.close();
	}

	@Test
	public void InsertCNote(){
		String sql = ": INSERT INTO `CNote` (caseNr,username,note) VALUES (1,'Bond','old men'),(1,'Sherlock','Dead Bodies Everywhere'),(2,'Bond',''),(2,'Sherlock','OMG so brutal'),(3,'Sherlock','Found fingerprints, could be from Lausbub Michi'),(4,'Bond','nice car'),(5,'Bond',''),(5,'kudi','shortly after midnight.'),(6,'Bond',''),(6,'kudi','maybe the CIA'),(7,'kudi','billion trade'),(8,'Bond','golden iphone'),(8,'kudi',''),(9,'kudi',''),(10,'Bond',''),(10,'kudi','huuge fire!'),(11,'demo','dsa');";
		Operator op = parser.parseSQL(sql);
		while (op.moveNext());
		op.close();
	}
	
	@Test
	public void InsertPoi(){
		String sql = ": INSERT INTO `Poi` (poiId,lastName,firstName,birthdate) VALUES (1,'Laufenberg','Felix','1027488000000'),(2,'Lausbub','Michi','967488000000'),(3,'Taube','Tim','1117488000000'),(4,'Smith','Will','867488000000'),(5,'Hanks','Tom','767488000000'),(6,'Damon','Matt','1000488000000'),(7,'demo','demo','1111488000000');";
		Operator op = parser.parseSQL(sql);
		while (op.moveNext());
		op.close();
	}
	
	@Test
	public void InsertConviction(){
		String sql = ": INSERT INTO `Conviction` (endDate,date,reason,poiId,caseNr) VALUES ('1297488000000','1267488000000','selfish',1,1),(NULL,'1267488000000,'',2,3),(NULL,'1267488000000','',3,3),(NULL,'1267488000000','',3,8),('1367488000000','1267488000000','-',5,1);";
		Operator op = parser.parseSQL(sql);
		while (op.moveNext());
		op.close();
	}
	
	@Test
	public void InsertCategory(){
		String sql = ": INSERT INTO `Category` (name,super) VALUES ('Crimes Against Property',NULL),('Drug related',NULL),('Personal Crimes',NULL),('sub',NULL),('Traffic related',NULL),('White Collar Crimes',NULL),('Arson','Crimes Against Property'),('Auto theft','Crimes Against Property'),('Burglary','Crimes Against Property'),('Drug trade','Drug related'),('Illegal Drug Use','Drug related'),('Assault','Personal Crimes'),('Murder','Personal Crimes'),('Robbery','Personal Crimes'),('sub2','sub'),('Hit and run','Traffic related'),('Speeding','Traffic related'),('Embezzling','White Collar Crimes'),('Insider trading','White Collar Crimes'),('Tax evasion','White Collar Crimes');";
		Operator op = parser.parseSQL(sql);
		while (op.moveNext());
		op.close();
	}

	@Test
	public void InsertInvolved(){
		String sql = ": INSERT INTO `Involved` (poiId,caseNr) VALUES (1,1),(5,1),(2,2),(2,3),(3,3),(4,4),(6,5),(4,7),(5,7),(3,8),(1,10),(1,11),(6,11);";
		Operator op = parser.parseSQL(sql);
		while (op.moveNext());
		op.close();
	}	
}
	