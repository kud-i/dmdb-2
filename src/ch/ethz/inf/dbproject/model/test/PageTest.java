package ch.ethz.inf.dbproject.model.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import ch.ethz.inf.dbproject.model.simpleDatabase.*;
import ch.ethz.inf.dbproject.model.simpleDatabase.operators.*;
import ch.ethz.inf.dbproject.model.simpleDatabase.storage.*;

/**
 * Unit tests for Part 2.
 * @author Martin Hentschel
 */
public class PageTest {
	private final TableManager tm;
	private TupleSchema schema1 = new TupleSchema(new String[] { "id", "col11", "col12" }, new String[] { "id" }, new String[] { "id" });
	private TupleSchema schema2 = new TupleSchema(new String[] { "id", "col11", "col12" }, new String[] { "id" }, new String[] { "id" });
	private List<Tuple> tuples1 = new LinkedList<Tuple>();
	{
		tuples1.add(new Tuple(schema1, new String[] { "1", "Car 1", "open" }));
		tuples1.add(new Tuple(schema1, new String[] { "2", "Car 2", "closed" }));
		tuples1.add(new Tuple(schema1, new String[] { "3", "Car 3", "open" }));
		tuples1.add(new Tuple(schema1, new String[] { "4", "Car 4", "closed" }));
	}

	private PageManager table1;

	public PageTest() throws IOException {
		delete(new File("tdb"));
		tm = new TableManager("tdb");
		tm.createTable("table1", schema1);
		tm.flush();
		table1 = tm.getTable("table1");
	}
	
	public static void delete(File f) throws IOException {
	  if (f.isDirectory()) {
	    for (File c : f.listFiles()) {
	      delete(c);
	    }
	  }
	  f.delete();
	}
	
	@Before
	public void purgeTable() throws IOException {
		table1.purge();
	}
	
	@Test
	public void testCreate() throws IOException {
		PageManager read = tm.getTable("table1");
		assertEquals(read.schema, schema1);
	}

	@Test
	public void testSimpleInsert() throws IOException, AlreadyExistException {
		Tuple t = tuples1.get(0);
		Page p = table1.getInsertPage(t);
		int slotNr = p.prepareAndAppend(t).getSlotNr();
	    assertEquals(0, slotNr);
	    table1.flush();
	    Page loadedPage = table1.load(p.pageNr, false);
	    Tuple loadedTuple = loadedPage.getSlot(slotNr);
	    assertEquals(t, loadedTuple);
	}
	
	@Test(expected = AlreadyExistException.class)
	public void testFailedInsert() throws IOException, AlreadyExistException {
		Tuple t = tuples1.get(0);
		Page p = table1.getInsertPage(t);
		int slotNr = p.prepareAndAppend(t).getSlotNr();
	    assertEquals(0, slotNr);
	    table1.getInsertPage(t);
	    p.prepareAndAppend(t);
	}

	@Test
	public void testScan() throws IOException, AlreadyExistException {
		// Hier wird geprüft, ob der Scan über alle manuell eingefügten Tuples funktioniert
		for(Tuple t: tuples1) {
			Page p = table1.getInsertPage(t);
			p.prepareAndAppend(t);
		}
		table1.flush();
		
		Operator op = new PagedScan(table1);
		String expected = "1,Car 1,open 2,Car 2,closed 3,Car 3,open 4,Car 4,closed";
		String actual = Part2Test.concatTuples(op);
		assertEquals(expected, actual);
	}

	@Test
	public void testSimpleDelete() throws IOException, AlreadyExistException {
		for(Tuple t: tuples1) {
			Page p = table1.getInsertPage(t);
			p.prepareAndAppend(t);
		}
		Page p = table1.load(0, false);
		p.delete(1);
		{
			Operator op = new PagedScan(table1);
			String expected = "1,Car 1,open 3,Car 3,open 4,Car 4,closed";
			String actual = Part2Test.concatTuples(op);
			assertEquals(expected, actual);
		}
	}
	
	@Test
	public void testInsert() throws IOException, AlreadyExistException {
		for(Tuple t: tuples1) {
			Page p = table1.getInsertPage(t);
			p.prepareAndAppend(t);
		}
		PageManager table2 = tm.createTable("table2", schema2);
		{
			Operator op = new Insert(
					new Select(new PagedScan(table1), "col11", "Car 2", SelectorFuncs.EQUAL),
					table2);
			assertTrue(op.moveNext());
			Tuple t = op.current();
			assertEquals(3, t.getValues().length);
			assertEquals(2, t.getInt("id"));
			assertEquals(0, t.getInt("pageNr"));
			assertEquals(0, t.getInt("slotNr"));
		}
		{
			Operator op = new PagedScan(table2);
			String expected = "2,Car 2,closed";
			String actual = Part2Test.concatTuples(op);
			assertEquals(expected, actual);
		}
		{
			TupleSchema adhoc = new TupleSchema(new String[] { "col11", "col12" });
			Operator op = new Insert(new Scan("(Car 3,open)", adhoc), table2, adhoc);
			assertTrue(op.moveNext());
			Tuple t = op.current();
			assertEquals(3, t.getValues().length);
			assertEquals(3, t.getInt("id"));
			assertEquals(0, t.getInt("pageNr"));
			assertEquals(1, t.getInt("slotNr"));
		}
		{
			Operator op = new PagedScan(table2);
			String expected = "2,Car 2,closed 3,Car 3,open";
			String actual = Part2Test.concatTuples(op);
			assertEquals(expected, actual);
		}
	}
	
	@Test
	public void testReplace() throws IOException, AlreadyExistException {
		for(Tuple t: tuples1) {
			Page p = table1.getInsertPage(t);
			p.prepareAndAppend(t);
		}
		{
			Operator op = new Replace(new Scan("(2,Car 2,open),(3,Car 3,closed),(4,Car 4,closed)", schema1), table1);
			assertTrue(op.moveNext());
			assertTrue(op.moveNext());
			assertTrue(op.moveNext());
		}
		{
			Operator op = new PagedScan(table1);
			String expected = "1,Car 1,open 2,Car 2,open 3,Car 3,closed 4,Car 4,closed";
			String actual = Part2Test.concatTuples(op);
			assertEquals(expected, actual);
		}
	}
	
	@Test
	public void testUpdate() throws IOException, AlreadyExistException {
		for(Tuple t: tuples1) {
			Page p = table1.getInsertPage(t);
			p.prepareAndAppend(t);
		}
		{
			List<Updator> updators = new LinkedList<Updator>();
			updators.add(new Updator("col11", "UCar"));
			Operator op = new Update(new PagedScan(table1), updators, table1);
			assertTrue(op.moveNext());
			assertTrue(op.moveNext());
			assertTrue(op.moveNext());
			assertTrue(op.moveNext());
		}
		{
			Operator op = new PagedScan(table1);
			String expected = "1,UCar,open 2,UCar,closed 3,UCar,open 4,UCar,closed";
			String actual = Part2Test.concatTuples(op);
			assertEquals(expected, actual);
		}
	}

	@Test
	public void testDelete() throws IOException, AlreadyExistException {
		for(Tuple t: tuples1) {
			Page p = table1.getInsertPage(t);
			p.prepareAndAppend(t);
		}
		{
			Operator op = new Delete(new Select(new PagedScan(table1), "col11", "Car 2", SelectorFuncs.EQUAL), table1);
			assertTrue(op.moveNext());
		}
		{
			Operator op = new PagedScan(table1);
			String expected = "1,Car 1,open 3,Car 3,open 4,Car 4,closed";
			String actual = Part2Test.concatTuples(op);
			assertEquals(expected, actual);
		}
	}

	@Test
	public void testForward() throws IOException, AlreadyExistException {
		StringBuffer longStringB = new StringBuffer();
		for(int i = 0; i < 1000; i++) {
			longStringB.append(i % 10);
		}
		String longString = longStringB.toString();
		StringBuffer buff = new StringBuffer();
		for(int i = 0; i < 10; i++) {
			Tuple t = new Tuple(schema1, new String[] { Integer.toString(i), longString, "open" });
			Page p = table1.getInsertPage(t);
			p.prepareAndAppend(t);
			if(i != 1) {
				buff.append(i + "," + longString + ",open ");
			}
		}
		buff.append("1," + longString + "," + longString);
		{
			Operator op = new Replace(new Scan("(1," + longString + "," + longString + ")", schema1), table1);
			assertTrue(op.moveNext());
		}
		{
			Operator op = new PagedScan(table1);
			String expected = buff.toString();
			String actual = Part2Test.concatTuples(op);
			assertEquals(expected, actual);
		}
	}
}
