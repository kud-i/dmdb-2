package ch.ethz.inf.dbproject.model.test;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import ch.ethz.inf.dbproject.model.Case;
import ch.ethz.inf.dbproject.model.Comment;
import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleSchema;
import ch.ethz.inf.dbproject.model.simpleDatabase.storage.TableManager;


public class ParserTest {
	private final DatastoreInterface dbInterface = new DatastoreInterface();
	private TupleSchema schema = new TupleSchema(new String[] { "poiId", "username", "note" });

	@Test
	public void testgetCaseByNr(){
		Case c = this.dbInterface.getCaseByNr(2);	
	}
	
	@Test
	public void testgetCasesbyPoiId(){
		List<Case> cs = this.dbInterface.getCasesByPoiId(2);
	}
	
	@Test
	public void testgetPNotebyPoiIdAndUser(){
	Comment s = this.dbInterface.getPNotebyPoiIdAndUser(2, "kudi");
	
	String expected = "kudi, timmy";
	String actual = s.getUsername() + ", " + s.getComment();
	assertEquals(expected, actual);
	
	}
	
	@Test
	public void test(){
		this.dbInterface.deleteCategoryByName("timmy");
	}
	
	@Test
	public void test2(){
		Case c = null;
		this.dbInterface.insertConvictionsForClosedCase(c);
	}
	
	@Test
	public void testUpdate(){
		this.dbInterface.updateCNote(2, "test", "test2");
	}
	
	@Test
	public void testSearchDate(){
		this.dbInterface.searchForCase("2010-03-02");
	}
	
}
