package ch.ethz.inf.dbproject.model.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import ch.ethz.inf.dbproject.model.Case;
import ch.ethz.inf.dbproject.model.simpleDatabase.*;
import ch.ethz.inf.dbproject.model.simpleDatabase.operators.*;

/**
 * Unit tests for Part 2.
 * @author Martin Hentschel
 */
public class Part2Test {

	private String[] columnNames = new String[] { "id", "name", "status", "nrOfSuspects" };
	private TupleSchema schema = new TupleSchema(columnNames);
	private TupleSchema schema1 = new TupleSchema(new String[] { "id", "name", "status" });
	private TupleSchema schema2 = new TupleSchema(new String[] { "id", "extra" });
	private String relation = "(1,Stolen car,open),(2,Fiscal fraud,closed),(3,High speed,open)";
	private String relation2 = "(1,stolen),(2,fraud),(3,speed)";
	private String relation3 = "(1,Stolen car,open,3),(2,Fiscal fraud,closed,5),(3,Stolen car,open,12)";
	private String relation4 = "(1,Stolen car,open,1),(2,Fiscal fraud,closed,1),(3,High speed,open,1)";
	private String relation5 = "(1,Stolen car,open,1),(2,Fiscal fraud,closed,1),(3,Stolen car,open,1)";
	private String relation6 = "(1,Stolen car,open),(2,Fiscal fraud,closed),(3,High speed,open),(4,Murder,open),(5,Theft,closed)";
	private String relation7 = "(1,Stolen car,open),(2,Fiscal fraud,),(3,High speed,open),(4,Murder,open),(5,Theft,closed)";

	@Test
	public void testProjectFunc() {
		Operator op = new Project(new Scan(relation, schema), new String[] { "name", "ZERO()" });
		String actual = concatTuples(op);
		String expected = "Stolen car,0 Fiscal fraud,0 High speed,0";
		assertEquals(expected, actual);
	}
	
	@Test
	public void testSelfJoin() {
		ArrayList<JoinPair<String>> join = new ArrayList<JoinPair<String>>();
		join.add(new JoinPair<String>("id", "id"));
		Operator op = new Join(
				new Scan(relation4, schema),
				new Scan(relation4, schema),
				join);
		String actual = concatTuples(op);
		String expected = "1,Stolen car,open,1,Stolen car,open,1 2,Fiscal fraud,closed,1,Fiscal fraud,closed,1 3,High speed,open,1,High speed,open,1";
		assertEquals(expected, actual);
	}

	@Test
	public void testSortMergeJoin() {
		{
			ArrayList<JoinPair<String>> join = new ArrayList<JoinPair<String>>();
			join.add(new JoinPair<String>("id", "id"));
			Operator op = new SortMerge(
					new Scan(relation, schema1),
					new Scan(relation2, schema2),
					join);
			String actual = concatTuples(op);
			String expected = "1,Stolen car,open,stolen 2,Fiscal fraud,closed,fraud 3,High speed,open,speed";
			assertEquals(expected, actual);
		}
		{
			ArrayList<JoinPair<String>> join = new ArrayList<JoinPair<String>>();
			join.add(new JoinPair<String>("id", "name"));
			TupleSchema leftSchema = new TupleSchema(new String[] { "id", "name" });
			TupleSchema rightSchema = new TupleSchema(new String[] { "name", "open" });
			String left = "(1,Stolen car),(2,Stolen car),(2,Fiscal fraud),(2,High speed),(1,Fiscal fraud),(1,High speed)";
			String right = "(1,open),(2,closed)";
			Operator op = new SortMerge(
					new Scan(left, leftSchema),
					new Scan(right, rightSchema),
					join);
			String actual = concatTuples(op);
			String expected = "1,High speed,1,open 1,Fiscal fraud,1,open 1,Stolen car,1,open " + 
							  "2,High speed,2,closed 2,Fiscal fraud,2,closed 2,Stolen car,2,closed";
			assertEquals(expected, actual);
		}
	}

	@Test
	public void testSelfJoin2() {
		ArrayList<JoinPair<String>> join = new ArrayList<JoinPair<String>>();
		join.add(new JoinPair<String>("status", "status"));
		Operator op = new Join(
				new Scan(relation4, columnNames),
				new Scan(relation4, columnNames),
				join);
		String actual = concatTuples(op);
		String expected = "1,Stolen car,open,1,1,Stolen car,1 1,Stolen car,open,1,3,High speed,1 2,Fiscal fraud,closed,1,2,Fiscal fraud,1 3,High speed,open,1,1,Stolen car,1 3,High speed,open,1,3,High speed,1";
		assertEquals(expected, actual);
	}
	
	@Test
	public void testJoin() {
		ArrayList<JoinPair<String>> join = new ArrayList<JoinPair<String>>();
		join.add(new JoinPair<String>("name", "name"));
		join.add(new JoinPair<String>("status", "status"));
		Operator op = new Join(
				new Scan(relation4, columnNames),
				new Scan(relation5, columnNames),
				join);
		String actual = concatTuples(op);
		String expected = "1,Stolen car,open,1,1,1 1,Stolen car,open,1,3,1 2,Fiscal fraud,closed,1,2,1";
		assertEquals(expected, actual);
	}
	
	@Test
	public void testJoin2() {
		ArrayList<JoinPair<String>> join = new ArrayList<JoinPair<String>>();
		join.add(new JoinPair<String>("name", "name"));
		join.add(new JoinPair<String>("status", "status"));
		Operator op = new Join(
				new Scan(relation4, schema),
				new Scan(relation5, schema),
				join);
		String actual = concatTuples(op);
		String expected = "1,Stolen car,open,1,1,1 1,Stolen car,open,1,3,1 2,Fiscal fraud,closed,1,2,1";
		assertEquals(expected, actual);
	}
	
	
	@Test
	public void testSort(){
		TupleSchema schema = new TupleSchema(new String[] { "id", "name" });
		String s = "(1,Stolen car),(2,Stolen car),(2,Fiscal fraud),(2,High speed),(1,Fiscal fraud),(1,High speed)";
		List<Comparator<Tuple>> funcs = new LinkedList<Comparator<Tuple>>();
		funcs.add(new SortFunc("id", true));
		Operator op = new Sort(new Scan(s, schema), new LexicographicSort(funcs));
		String actual = concatTuples(op);
		String expected = "1,Stolen car 1,Fiscal fraud 1,High speed 2,Stolen car 2,Fiscal fraud 2,High speed";
		assertEquals(expected, actual);
	}

	@Test
	public void testScan() {
		Operator op = new Scan(relation, schema);
		String expected = "1,Stolen car,open, 2,Fiscal fraud,closed, 3,High speed,open,";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}

	@Test
	public void testLimit() {
		Operator op = new Limit(new Scan((relation), schema), 2);
		String expected = "1,Stolen car,open, 2,Fiscal fraud,closed,";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}

	@Test
	public void testSelect() {
		Operator op = new Select(new Scan((relation), schema), "status", "closed", SelectorFuncs.EQUAL);
		String expected = "2,Fiscal fraud,closed,";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testNullSelect() {
		Operator op = new Select(new Scan((relation7), schema), "status", "", SelectorFuncs.EQUAL);
		String expected = "2,Fiscal fraud,,";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testLikeSelect() {
		Operator op = new Select(new Scan((relation), schema), "status", "clo.*", SelectorFuncs.LIKE);
		String expected = "2,Fiscal fraud,closed,";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testLikeOrSelect() {
		String[] args = {".*ed.*",".*ed.*"};
		String[] columns = {"status","name"};
		Operator op = new SelectLikeOr(new Scan((relation), schema), columns , args);
		String expected = "2,Fiscal fraud,closed, 3,High speed,open,";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testLikeSelect2() {
		Operator op = new Select(new Scan((relation), schema), "status", ".*lo.*", SelectorFuncs.LIKE);
		String expected = "2,Fiscal fraud,closed,";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testLikeSelect3() {
		// Test case insensitivity
		Operator op = new Select(new Scan((relation), schema), "status", ".*O.*", SelectorFuncs.LIKE);
		String expected = "1,Stolen car,open, 2,Fiscal fraud,closed, 3,High speed,open,";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}
	@Test
	public void testProjectByName() {
		Operator op = new Project(new Scan(relation, schema), "name");
		String expected = "Stolen car Fiscal fraud High speed";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}

	@Test
	public void testProjectByIdStatus() {
		String[] columns = new String[] { "status", "id" };
		Operator op = new Project(new Scan((relation), schema), columns);
		String expected = "open,1 closed,2 open,3";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}

	@Test
	public void testSelectProject() {
		Operator op = new Project(new Select(new Scan((relation), schema), "status", "closed", SelectorFuncs.EQUAL), "name");
		String expected = "Fiscal fraud";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}

	/**
	 * Concatenates all tuples returned by the operator. The tuples are separated
	 * by a simple space.
	 * @param op operator to read from
	 * @return concatenated tuples
	 */
	public static String concatTuples(Operator op) {
		StringBuilder buf = new StringBuilder();
		while (op.moveNext()) {
			buf.append(op.current().toString());
			buf.append(" ");
		}
		if(buf.length() > 0) {
			// delete last space
			buf.deleteCharAt(buf.length() - 1);
		}
		return buf.toString();
	}
	
	@Test
	public void testGroupSum() {
		String[] columns = {"status"};
		AggregateFuncs[] aggregateFuncs = {AggregateFuncs.SUM};
		String[] args = {"nrOfSuspects"};
		Operator op = new GroupBy((new Scan((relation3), schema)), columns, aggregateFuncs, args);
		String expected = "open,15 closed,5";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGroupAvg() {
		String[] columns = {"status"};
		AggregateFuncs[] aggregateFuncs = {AggregateFuncs.AVERAGE};
		String[] args = {"nrOfSuspects"};
		Operator op = new GroupBy((new Scan((relation3), schema)), columns, aggregateFuncs, args);
		String expected = "open,7 closed,5";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGroupCount() {
		String[] columns = {"status"};
		AggregateFuncs[] aggregateFuncs = {AggregateFuncs.COUNT};
		String[] args = {"nrOfSuspects"};
		Operator op = new GroupBy((new Scan((relation3), schema)), columns, aggregateFuncs, args);
		String expected = "open,2 closed,1";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGroupCountSum() {
		String[] columns = {"status"};
		AggregateFuncs[] aggregateFuncs = {AggregateFuncs.COUNT,AggregateFuncs.SUM};
		String[] args = {"id","nrOfSuspects"};
		Operator op = new GroupBy((new Scan((relation3), schema)), columns, aggregateFuncs, args);
		String expected = "open,2,15 closed,1,5";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGroupCountSumAvg() {
		String[] columns = {"status"};
		AggregateFuncs[] aggregateFuncs = {AggregateFuncs.COUNT,AggregateFuncs.SUM,AggregateFuncs.SUM};
		String[] args = {"name","id","nrOfSuspects"};
		Operator op = new GroupBy((new Scan((relation3), schema)), columns, aggregateFuncs, args);
		String expected = "open,2,4,15 closed,1,2,5";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGroupNone() {
		String[] columns = {"status"};
		AggregateFuncs[] aggregateFuncs = new AggregateFuncs[0];
		String[] args = new String[0];
		Operator op = new GroupBy(new Scan(relation3, schema), columns, aggregateFuncs, args);
		String expected = "open closed";
		String actual = concatTuples(op);
		assertEquals(expected, actual);
	}

	@Test
	public void testCreatefromTuple() {
		Case test1_ = new Case(2,"bla","blubb",true,new Date(1000000000),"du","da","do");
		String columns_[] = {"caseNr", "categoryName", "title","open",
				"date", "location", "description", "username"};
		TupleSchema schema = new TupleSchema(columns_);
		String args[] = {"2","bla","blubb","true","1000000000","du","da","do"};
		Tuple testTuple = new Tuple(schema,args);
		Case test2_ = new Case(testTuple);
		String test1 = Integer.toString(test1_.getCaseNr()) + " " + test1_.getCategoryName() + " " + test1_.getTitle()
				+ " " + test1_.getOpen() + " " + test1_.getDate().toString() + " " + test1_.getLocation() + " " + test1_.getDescription() 
				+ " " + test1_.getUsername();
		String test2 = Integer.toString(test1_.getCaseNr()) + " " + test1_.getCategoryName() + " " + test1_.getTitle()
				+ " " + test1_.getOpen() + " " + test1_.getDate().toString() + " " + test1_.getLocation() + " " + test1_.getDescription() 
				+ " " + test1_.getUsername();
		assertEquals(test1,test2);
	}
	
	@Test
	public void testNotIn1() {
		String[] columns = new String[] { "status", "id" };
		Operator opL = new Project(new Scan((relation), schema), columns);
		Operator opR = new Select(new Scan((relation), schema), "status", "closed", SelectorFuncs.EQUAL);
		Operator test = new NotIn(opL,opR,"id","id");
		String expected = "open,1 open,3";
		String actual = concatTuples(test);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testNotIn2() {
		String[] columns = new String[] { "status", "id" };
		Operator opL = new Project(new Scan(relation6, columnNames), columns);
		Operator opR = new Select(new Scan(relation6, columnNames), "status", "closed", SelectorFuncs.NOT_EQUAL);
		Operator test = new NotIn(opL,opR,"id","id");
		String expected = "closed,2 closed,5";
		String actual = concatTuples(test);
		assertEquals(expected, actual);
	}
}
