package ch.ethz.inf.dbproject.model.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import ch.ethz.inf.dbproject.model.simpleDatabase.*;
import ch.ethz.inf.dbproject.model.simpleDatabase.operators.*;
import ch.ethz.inf.dbproject.model.simpleDatabase.storage.*;

/**
 * Unit tests for Part 2.
 * @author Martin Hentschel
 */
public class PerformanceTest {
	private final TupleSchema schema;
	private TableManager tm;
	private Tuple t;
	
	private final int rows = 100000;
	{
		// first 10 cols (0..9) are auto increment,
		// cols 5..9 are keys
		String[] icols = new String[90];
		String[] vals = new String[90];
		String[] cols = new String[100];
		String[] incs = new String[10];
		String[] primary = new String[10];
		for(int i = 0; i < cols.length; i++) {
			cols[i] = "col" + i;
		}
		for(int i = 0; i < primary.length; i++) {
			primary[i] = "col" + (i + 5);
		}
		for(int i = 0; i < incs.length; i++) {
			incs[i] = "col" + i;
		}
		for(int i = 0; i < vals.length; i++) {
			vals[i] = "value" + i;
			icols[i] = "col" + (i + 10);
		}
		schema = new TupleSchema(cols, primary, incs);
		
		try {
			PageTest.delete(new File("tdb"));
			tm = new TableManager("tdb");
			tm.createTable("table", schema);
			tm.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		t = new Tuple(new TupleSchema(icols), vals);
	}

	@Before
	public void purgeTable() throws IOException {
		tm.getTable("table").purge();
	}

	@Test
	public void testPerformance() throws IOException {
		PageManager table = tm.getTable("table");
		List<Tuple> tuples = new LinkedList<Tuple>();
		{
			long startMillis = System.currentTimeMillis();
			for(int i = 0; i < rows; i++) {
				tuples.add(t);
			}
			Operator op = new Insert(new BufferedOp(tuples), table, t.getSchema());
			for(int i = 0; i < rows; i++) {
				assertTrue(op.moveNext());
				assertEquals(12, op.current().getValues().length);
			}
			op.close();
			long endMillis = System.currentTimeMillis();
			System.out.println("INSERT: " + (endMillis - startMillis));
			assertTrue((endMillis - startMillis) <= 10000);
		}{
			long startMillis = System.currentTimeMillis();
			for(int i = 0; i < rows; i++) {
				tuples.add(t);
			}
			Operator op = new PagedScan(table);
			for(int i = 0; i < rows; i++) {
				assertTrue(op.moveNext());
				assertEquals(100, op.current().getValues().length);
			}
			op.close();
			long endMillis = System.currentTimeMillis();
			System.out.println("SCAN: " + (endMillis - startMillis));
			assertTrue((endMillis - startMillis) <= 1000);
		}{
			long startMillis = System.currentTimeMillis();
			for(int i = 0; i < rows; i++) {
				tuples.add(t);
			}
			Operator op = new Select(new PagedScan(table), "col1", Integer.toString(rows - 1), SelectorFuncs.EQUAL);
			assertTrue(op.moveNext());
			assertEquals(100, op.current().getValues().length);
			assertEquals(rows - 1, op.current().getInt(0));
			assertFalse(op.moveNext());
			op.close();
			long endMillis = System.currentTimeMillis();
			System.out.println("SELECT: " + (endMillis - startMillis));
			assertTrue((endMillis - startMillis) <= 1000);
		}{
			long startMillis = System.currentTimeMillis();
			for(int i = 0; i < rows; i++) {
				tuples.add(t);
			}
			String[] vals = new String[10];
			for(int i = 0; i < 5; i++) {
				vals[i] = "100";
			}
			for(int i = 5; i < 10; i++) {
				vals[i] = "value" + (i - 5);
			}
			Operator op = new IndexSelect(table, vals);
			assertTrue(op.moveNext());
			assertEquals(100, op.current().getValues().length);
			long endMillis = System.currentTimeMillis();
			for(int i = 0; i < 10; i++) {
				assertEquals(100, op.current().getInt(i));
			}
			assertFalse(op.moveNext());
			op.close();
			System.out.println("SELECT: " + (endMillis - startMillis));
			assertTrue((endMillis - startMillis) <= 10);
		}
	}
}
