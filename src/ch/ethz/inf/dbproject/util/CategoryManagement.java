package ch.ethz.inf.dbproject.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import ch.ethz.inf.dbproject.model.Category;
import ch.ethz.inf.dbproject.model.DatastoreInterface;

public class CategoryManagement {
	private static final DatastoreInterface db = new DatastoreInterface();
	
	public static final List<Category> getCategories() {
		return deepSearchCategories(0, null);
	}

	private static final List<Category> deepSearchCategories(int level, Category current) {
		List<Category> categories = current == null ?
			db.getRootCategories() :
			db.getSubCategories(current);
	
		List<Category> newList = new LinkedList<Category>();
		for(Category c: categories) {
			c.setLevel(level + 1);
			newList.add(c);
			newList.addAll(deepSearchCategories(level + 1, c));
		}
		return newList;
	}
}
